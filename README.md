# Framework for developing the accesibility of subclonal reconstruction tools
The framework presented consists of 3 main components

- Data generation
- Data transformation
- Tool analysis

It is primarily programmed in Python, but will contain some R and Bash.
The tools are integrated into the framework.
The goal of the framework is to get an holistic overview of the tools responses to particular input scenarios.