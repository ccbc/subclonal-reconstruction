if __name__ == "__main__":
  #This is the interface that gives easy acces to running the simulations and subsequent pipelines
  import os;
  import sys;
  import argparse;
  import re;
  import numpy as np;
  from settings import settings;
  from main import main;
  from multiprocessing import Pool;
  from random import randint;
  
  #for the reporting
  import matplotlib;
  matplotlib.use("Agg");
  import matplotlib.pyplot as plt;
  from pylab import savefig;
  from make_report import PDF;
  
  rrg_path = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/";
  
  #import from programpipes
  sys.path.append(rrg_path + "programpipes");
  from getData import *;
  
  from time import time;
  from datetime import datetime;
  
  SHOW_ELAB = 0;
  def show(*args,**kwargs):
    sys.stdout = sys.__stdout__;
    time_stamp = datetime.now().replace(second=0,microsecond=0).strftime('%Y-%m-%d %H:%M');
    print("RUN - ",time_stamp,": ",*args,**kwargs, sep = "");
    
  pipes = ["TargetClone","DPClust","PhyloWGS","SubClonalSelection","SciClone","QuantumClone","Mobster"];
  pipe_ids = ["tc","dpc","pwgs","scs","sci","quc","mob"];
  
  # Initiate the argument parser
  parser = argparse.ArgumentParser();
  parser.add_argument("-p", "--program", help="The program pipeline to run");
  parser.add_argument("-g", "--genome_size", help="The genome size (number of nucleotides) of the cells simulated. - \
                                                   default = '1000' - \
                                                   example = '750' \
                                                   for a genome size of 750 nucleotides for each cell");
  parser.add_argument("-m", "--mutload", help="Parameter - The mutation load (fraction of mutated nucleotides) \
                                               of subclones. - \
                                               provide '<fraction of subclone genome mutated>:\
                                               <total steps>:<final fraction>', \
                                               default = '0.005' - \
                                               example = '0.004:5:0.020' results in \
                                               a parameter space of (0.004,0.008,0.012,0.016,0.020) for the \
                                               subclone mutation fraction");
  parser.add_argument("-x", "--mcmc_samples", help="Parameter - The mcmc_samples used by tool \
                                               for inferrence");
  parser.add_argument("-w", "--subclone_stdv", help="Parameter - The expansion fraction's standard deviation \
                                                     , the random spread in the fraction of he population \
                                                     that it will take over.");
  parser.add_argument("-c", "--subclone_size", help="Parameter - The expansion fraction of a subclone, \
                                                     the fraction of the population that it takes over.");
  parser.add_argument("-n", "--sample_number", help="Parameter - The number of samples that will be taken during \
                                                     simulation, these will then be used in the analysis step \
                                                     (the pipelined program). - \
                                                     Keep to maximum of 5 - \
                                                     example = '1:3:3' will result in 3 samples to be analysed");
  parser.add_argument("-q", "--seq_noise", help="Parameter - The simulated sequencing noise (gaussian noise \
                                                 imposed on the VAFs).");
  parser.add_argument("-i", "--identical", help="Make sure that the simulation are identical (same seed). \
                                                 All the stochastic determination will be equal for different\
                                                 parameter values.");
  parser.add_argument("-r", "--relations", help="Test specific class of phylogenetic relations \
                                                 (unchanged per parameter). \
                                                 For 3 subclones (6 possible phylogenies) keep them constant \
                                                 throughout run. [indicate 1] \
                                                 For 4 subclones (24 possible phylogenies keep specific group/class\
                                                 constant throughout run. [inidicate 1-4]");
  parser.add_argument("-l", "--loops", help="The amount of simulations to run");
  parser.add_argument("-s", "--subloops", help="The amount of analyses to run on each simulation");
  args = parser.parse_args();
  
  #check the pipeline to run
  run_pipe_id_list = [];
  if args.program:
    if args.program in pipes:
      run_pipe_id_list = [pipes.index(args.program)];
      
    elif args.program in pipe_ids:
      run_pipe_id_list = [pipe_ids.index(args.program)];
      
    elif all([pipe_id in pipe_ids for pipe_id in args.program.split(',')]):
      run_pipe_id_list = [pipe_ids.index(pipe_id) for pipe_id in args.program.split(',')];
      
    else:
      show("This pipeline(s) does not exist, try one of the following:\n","\n".join(list(map(str,zip(pipes,pipe_ids)))), sep="")
      sys.exit(2)
  else:
    show("Please add a pipeline, try one of the following:\n","\n".join(list(map(str,zip(pipes,pipe_ids)))), sep="")
    sys.exit(2);  
  #check for how many different simulations to run
  if args.loops:
    LOOPS = int(args.loops);
  else:
    LOOPS = 1;
  #check how many analysis per simulations to run
  if args.subloops:
    SUBLOOPS = int(args.subloops);
  else:
    SUBLOOPS = 1;
  
  #check if input is type TODO: use this to check input types
  def check(var,var_type):
    if not isinstance(var,var_type):
      show("Input error: ",var," is not a/an ",var_type);
      sys.exit(2);
  
  uisettings = settings();
  
  s = uisettings.rrg;
  d = uisettings.default_rrg;
  #Statics
  static_names = [];
  def set_static(arg_var,setting_name,func=int):
    '''requires dict objects: "s" (to be installed settings), and "d" (default settings)'''
    if arg_var:
      s[setting_name] = func(arg_var);
    else:
      s[setting_name] = d[setting_name];
  
  #Parameters
  parameter = None;
  parameter_setting = None;
  parameter_space = None;
  parameter_names = ["genome_size","mutload","subclone-size","subclone_stdv","mcmc_samples"]; #TODO: use this to assign it to parameter to identify the parameter chosen
  def set_parameter(arg_var,setting_name,parameter_name,func=int):
    global parameter, parameter_setting, parameter_space;
    
    if arg_var:
      if ':' in arg_var:
        if parameter != None:
          show("Not multiple parameters allowed: pick one parameter to sweep.");
          sys.exit(2);
        parameter = parameter_name;
        
        #construct parameter space
        range_strs = arg_var.split(':');
        step_n = int(range_strs[1]);
        range_vals = list(map(func,[range_strs[0],range_strs[2]]));
        range_step = (range_vals[1]-range_vals[0])/(step_n-1);
        parameter_space = [func(round(range_vals[0] + range_step*i,4)) for i in range(step_n)];
        
        #set to first paramter of parameter space
        parameter_setting = setting_name;
        s[setting_name] = parameter_space[0];
        return();
        
    set_static(arg_var,setting_name,func);
  
  #set parameters from CLI input
  set_parameter(args.genome_size,"nuclN","genome_size",func=int);
  set_parameter(args.mutload,"bigevent_snv_fraction","mutload",func=float);
  set_parameter(args.subclone_size,"bigevent_percentage","subclone_size",func=float);
  set_parameter(args.subclone_stdv,"bigevent_percentage_stdv","subclone_stdv",func=float);
  set_parameter(args.sample_number,"timepointN","sample_number",func=int);
  set_parameter(args.seq_noise,"seq_noise","seq_noise",func=float);
  set_parameter(args.mcmc_samples,"mcmc_samples","mcmc_samples",func=int);
  
  #if no parameter indicated do single run
  if parameter == None:
    parameter = "";
    parameter_space = [None];
    show("Single parameter setting run");
  else:
    show("Multi parameter setting run, for ",parameter," with range ",parameter_space);
  
  #Simulation and Phylogenies
  same_phylogeny = 0; # keep the phylogenies constant throughout the 
  same_simulation = 0;
  phylo_sc_n = len(uisettings.default_rrg["bigevent_times"]);
  if args.relations:
    same_phylogeny = 1;
    
    #3 subclones
    if phylo_sc_n == 3:
      if int(args.relations) == 1:
        #add simulation overloader (more simulations than phylogenies available)
        if LOOPS > 6:
          if LOOPS/6 != int(LOOPS/6):
            show("WARNING: please use multiple of 6 for 3-subclone-number phylogenies,\
                  otherwise some phylogenies are under-representated.");
          phylogs = ([[0,0,0],[0,0,1],[0,0,2],[0,1,0],[0,1,1],[0,1,2]]*(LOOPS//6+1))[:LOOPS];
        else:
          phylogs = [[0,0,0],[0,0,1],[0,0,2],[0,1,0],[0,1,1],[0,1,2]];
    #4 subclones
    elif phylo_sc_n == 4:
      #group 1:
      if int(args.relations) == 1:
       phylogs = [[0,0,0,0],[0,0,0,1],[0,0,0,2],[0,0,0,3],[0,0,1,0],[0,0,1,1],[0,0,1,2],[0,0,1,3]];
      #group 2:
      if int(args.relations) == 2:
       phylogs = [[0,0,2,0],[0,0,2,1],[0,0,2,2],[0,0,2,3],[0,1,0,1],[0,1,0,1],[0,1,0,2],[0,1,0,3]];
      #group 3:
      if int(args.relations) == 3:
       phylogs = [[0,1,1,0],[0,1,1,1],[0,1,1,2],[0,1,1,3],[0,1,2,0],[0,1,2,1],[0,1,2,2],[0,1,2,3]];
       
  if args.identical:
    same_simulation = 1;
  
  #log time (for execution time)
  starttime = time();
  
  #start pdf report
  pdf = PDF();
  pdf.set_author('Niels Burghoorn');
  pdf.set_title("Statistical analysis figures " + "for " + parameter);
  # pd.add_page();
  # pd.set_y(600);
  
  def ccfs_plot(par, loop, subloop, ccfs, width = 100, height = 100):
    x = list(range(len(ccfs[0])));
    for y in ccfs:
      plt.plot(x,y);
      
    fig_path = rrg_path + "output/process_" + str(loop) + "/";
    fig_name = "tmp_ui_" + str(par) + "_" + str(loop) + "_" + str(subloop) + ".png";
    
    savefig(fig_path+fig_name);
    plt.clf();
    
    return(fig_path, fig_name);
  
  def run_analyses(process_id, output_path, run_pipe_id):
    
    #LOOP4 - Different analyses
    
    subloop_results = [];
    for subloop in range(SUBLOOPS):
      #run pipeline (tool)
      subloop_runtime = time();
      
      os.system("python3 " + rrg_path + "programpipes/run" + pipes[run_pipe_id] + ".py "\
                + str(process_id) + " "\
                + str(uisettings.rrg["mcmc_samples"]) + " "\
                + str() + " > " + output_path + pipe_ids[run_pipe_id] + "_log.txt 2>&1");
      
      subloop_runtime = time() - subloop_runtime;
      
      #extract info
      def extract_pipe_info(run_pipe_id,process_id):
        if run_pipe_id == 0: #TargetClone
          show("NOT YET IMPLEMENTED");
          pass;
        elif run_pipe_id == 1: #DPClust
          sc_n,ccfs = get_dpclust(process_id);
          
        elif run_pipe_id == 2: #PhyloWGS
          sc_n,ccfs = get_phylowgs(process_id);
              
        elif run_pipe_id == 3: #SubClonalSelection
          show("NOT YET IMPLEMENTED");
          pass;
          
        elif run_pipe_id == 4: #SciClone
          sc_n,ccfs = get_sciclone(process_id);
          pass;
        
        elif run_pipe_id == 5: #QuantumClone
          sc_n,ccfs = get_quantumclone(process_id);
        
        elif run_pipe_id == 6: #MOBSTER
          show("NOT YET IMPLEMENTED");
          #sc_n,ccfs = get_mobster(process_id);
          pass
        
        return([sc_n,tuple(ccfs)]);
      
      result = extract_pipe_info(run_pipe_id,process_id) + [subloop_runtime];
      
      subloop_results.append(result);
      
      show("Pipeline: ",pipes[run_pipe_id]," loop_",process_id,"-estim._",subloop,": sc#=",result[0]);
      
    return(subloop_results);
  
  def loop_execute(loop_args):
    
    process_id, par, seed = loop_args;
    
    output_path = rrg_path+"output/process_"+str(process_id)+"/";
    if not os.path.exists(output_path):
      os.mkdir(output_path);
    
    show("Process_",process_id," seed = ",seed);
    
    #Take simulation info or Run simulation
    if same_phylogeny:
      s["bigevent_parents"] = phylogs[process_id];
    true_sc_n, true_ccfs = run_simulation(process_id, uisettings, par, output_path, seed);
    
    
    #LOOP3 - Different tools (pipes)
    if len(run_pipe_id_list) > 1:
      show("Run the pipelines: "+args.program);
      
    pipe_results = [];
    for run_pipe_id_index in run_pipe_id_list:
      
      #run one of the asked pipeline
      subloop_results = run_analyses(process_id, output_path, run_pipe_id_index);
      
      #every subloop_result contains (sc_n, ccfs)
      pipe_results.append(subloop_results);
    
    #add to the loop_results list, every pipeline_result contains 1 or more subloop_results
    return(pipe_results,(true_sc_n,true_ccfs));
  
  def run_simulation(process_id, uisettings, par, output_path, seed):
    #run simulation
    if SHOW_ELAB: show("Remove old output");
    os.system("rm -f "+ output_path + "*.txt");
    
    if SHOW_ELAB: show("Run the simulation");
    os.system("cat settings.py > " + output_path + "settings.txt");
    
    main(uisettings,process_id,seed);
    
    #add fishplot
    os.system("cat " + output_path + "prevalences.txt | python3 FractionsToCCFs.py | Rscript fishPlot.R " + str(process_id) + " " + str(par));
    
    #add ccf plots
    sample_number = int(uisettings.rrg["timepointN"]);
    for sampleID in range(sample_number):
      os.system("cat " + output_path + "reads_data_" + str(sampleID) +".txt | \
                 python3 ReadsToCCFs.py nozero | \
                 Rscript PlotVAFHist.R " + \
                 output_path + "density_" + str(sampleID) + "_" + str(process_id) + "_" + str(par) + ".png");
    os.system("cat " + output_path + "reads_data.txt | \
                 python3 " + rrg_path + "ReadsToCCFs.py nozero | \
                 Rscript " + rrg_path + "PlotVAFHist.R " + \
                 output_path + "density_" + str(sample_number) + "_" + str(process_id) + "_" + str(par) + ".png");
    
    #Gather 'truth' information from simuation for later error estimations
    true_sc_n, true_ccfs = find_true_values(output_path);
    
    return((true_sc_n, true_ccfs));
  
  def find_true_values(output_path):
    
    #true_sc_n contains the number of subclones
    with open(output_path + "settings.txt") as f:
      settings = f.read();
      
      sc_n_expr = r"bigevent_times = \[([0-9][^\]]*)";
      matches = re.findall(sc_n_expr,settings)[0].split(',');
    true_sc_n = len(matches)+1;
      
    #true_ccfs contain the ccf of each subclone at the sampled timepoints
    os.system("cat " + output_path + "prevalences.txt | python3 " + rrg_path + "FractionsToCCFs.py > " + output_path + "tmp_ui.txt")
    with open(output_path + "tmp_ui.txt") as f:
      lines = f.readlines();
      timepoints = list(map(lambda x : int(x)+2,lines[0].split(','))); #time points's line numbers at which was sampled
      if 10+2 not in timepoints: timepoints.append(10+2); #add the last time point's line number if not included
      
      true_ccfs = [[] for _ in range(true_sc_n)];
      
      #loop over timepoints's line numbers that are sampled and extract ccfs for later comparison 
      for timepoint in timepoints:
        ccf = list(map(float,lines[timepoint].split(',')));
        for sc in range(true_sc_n):
          true_ccfs[sc].append(ccf[sc]);
    os.system("rm -f "+ output_path + "tmp_ui.txt");
    
    return((true_sc_n,true_ccfs));
  
  #if indicated do not change anything but parameter (predefined simulations)
  def get_random_seed():
    return randint(0,10000);
  if same_simulation:
    seeds = [get_random_seed() for loop in range(LOOPS)];
  
  #the list with all the results
  par_results = [];
  true_values = [];
  sample_number = [];
  #LOOP1 - Different parameters
  for parID in range(len(parameter_space)):
    par = parameter_space[parID];
    if parameter != "":
      s[parameter_setting] = par;
      show("Next parameter value for ",parameter,": ",par);
    
    #register number of samples for the reports's ccf plots
    sample_number.append(int(s["timepointN"]));
    
    #LOOP2 - (PROCCES-SPLIT) Different simulations
    show("Running all loops");
    
    #for each simuation run a seperate process
    pool = Pool();
    if same_simulation:
      loop_results = pool.map(loop_execute,[(loop,par,seeds[loop]) for loop in range(LOOPS)]);
    else:
      loop_results = pool.map(loop_execute,[(loop,par,get_random_seed()) for loop in range(LOOPS)]);
    
    true_values.append([loop_result[1] for loop_result in loop_results])  
    par_results.append([loop_result[0] for loop_result in loop_results]);

  #make copy of true data
  par_true_results = true_values;
  
  #Analyse the data (find the number of correct subclone number estimations and the ccfs errors)
  results = np.asarray(par_results);
  true_values = np.asarray(true_values);
  
  PARS,LOOPS,PIPES,SUBLOOPS,DATAPOINTS = results.shape;

  #find all correct sc_n estimates
  sc_n_true = true_values[:,:,0];
  sc_n_results = results[:,:,:,:,0];
  true_compare = np.empty_like(sc_n_results);
  for i in range(PARS):
    for j in range(LOOPS):
      true_compare[i,j,:,:] = np.full((PIPES,SUBLOOPS),sc_n_true[i,j]);
  
  correct_estimates = (sc_n_results == true_compare);
  correct_estimates_pipe = np.sum(np.array(correct_estimates, dtype = int),axis = 3);
  correct_estimates_parpipe = np.sum(correct_estimates_pipe, axis = 1);
  
  #add page for each simulation
  for parID in range(len(parameter_space)):
    par = parameter_space[parID];
    for loop in range(LOOPS):
      fig_path = rrg_path + "output/process_" + str(loop) + "/";
      fig_fish_name = "fishPlot_" + str(loop) + "_" + str(par) + ".png";
      fig_ccfs_names = [fig_path + "density_" + str(sampleID) + "_" + str(loop) + "_" + str(par) + ".png" 
                        for sampleID in range(sample_number[parID]+1)];
      
      title = "process " + str(loop) + ", for " + parameter + str(par);
      pdf.simulation_page(
        title,
        fig_path + fig_fish_name,
        fig_ccfs_names,
        [pipes[pipe_id] for pipe_id in run_pipe_id_list],
        correct_estimates_pipe[parID,loop],
        correct_estimates_pipe[parID,loop],
        SUBLOOPS,
        );

  def stats_figure(stats, title,tag):
    stats_file_name = rrg_path +"output/tmp_ui_stats_"+tag+".png";
    
    fig = plt.figure(figsize = (10,5));
    plt.plot(stats);
    plt.title(title);
    savefig(stats_file_name);
    plt.clf();
    
    return(stats_file_name);
  
  def par_plot_figure(parameter_space, stats, title):
    stats_file_name = rrg_path +"output/par_plot.png";
    
    fig = plt.figure(figsize = (10,5));
    for stat in stats:
      plt.plot(parameter_space,stat);
    plt.title(title);
    plt.xlabel(parameter);
    plt.ylabel("score");
    savefig(stats_file_name);
    plt.clf();
    
    return(stats_file_name);
  
  if parameter != "":
    
    par_plot_name = par_plot_figure(parameter_space, correct_estimates_parpipe.T, 
                                    "all tool's preformances for given parameter values");
    
    pdf.parameter_page(
      "parameter_test",
      par_plot_name,
      [pipes[pipe_id] for pipe_id in run_pipe_id_list],
      parameter_space,
      correct_estimates_parpipe,
      correct_estimates_parpipe,
      LOOPS,
      SUBLOOPS
      );
    
    # #Show the output for all the parameters
    # show("\n\nALL PARAM OUT:\n");
    # for parID in range(len(parameter_space)):
    #   print(parameter," = ",parameter_space[parID]);
    #   print("correct subclone numbers: ",par_stats[parID][0]);
    #   print("average total error: ",par_stats[parID][1]);
    #   print();
    
    show("execution time: ",time()-starttime);
    starttime = time();
  
  # pdf.dump_text_page(results)
  # pdf.dump_text_page(true_values)
  
  np.save(rrg_path + "/output/pipe_estimations",results);
  np.save(rrg_path + "/output/true_values",true_values);
  
  # show(results);
  # pdf.dump_text_page(par_results)
  # pdf.dump_text_page(par_true_results)
  
  #save the output
  show("Save the output");
  pdf.output(rrg_path+"output/visual_report.pdf",'F');
  
  #remove lingering files
  os.system("rm -Rf "+ rrg_path + "output/process_*")
  
  show("execution time: ",time()-starttime);
