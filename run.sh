arg1="$1"
arg2="$2"

case $arg1 in
	"")
		rm -f ./output/*.txt;
		cat settings.py > output/settings.txt;
		python3 main.py;
		;;
	"init")
		rm -f ./output/*.txt;
		cat settings.py > output/settings.txt;
		;;
	"fish")
	#   not_spatial_samples=$(ls output/reads_data.txt | wc -l);
	# 	if [ not_spatial_samples -eq "1"]
	# 	then
		  cat output/prevalences.txt | python3 FractionsToCCFs.py | Rscript fishPlot.R;
		;;
	"density")
		if [ $# -eq 1 ]; then
		  cat output/reads_data.txt | python3 ReadsToCCFs.py nozero | Rscript PlotVAFHist.R;
		elif [ $# -eq 2 ]; then
			cat output/reads_data_${arg2}.txt | python3 ReadsToCCFs.py nozero | Rscript PlotVAFHist.R ${arg2};
		fi	
		;;
	"simtest")
		FILENUMBER=$(ls ../simtests -1 | wc -l);
		FILENUMBER2="$((FILENUMBER + 1))";
		cp -r output ../simtests/;
		mv ../simtests/output ../simtests/simtest_$FILENUMBER2;
		;;
	"targetclone")
		;;
	"pwgs")
		rm -f ./output/*.txt;
		python3 main.py;
		python3 ./programpipes/runPhyloWGS.py;
		;;
	"report")
		./run.sh fish;
		
		not_spatial_samples=$(ls output/reads_data.txt | wc -l);
		if [ $not_spatial_samples -eq 1 ]
		then
		  echo "not spatial"
		  ./run.sh density;
		fi
		
		read_files=$(ls output/reads_data_*.txt | wc -l);
		for read_fileID in $(seq 0 $(($read_files-1)))
		do
		  ./run.sh density $read_fileID;
		done
		
		python3 simulation_report.py $read_files;
		rm -f output/densityVAF*.png output/fishPlot.png;
esac
