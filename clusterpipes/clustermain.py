#main for clustering options
import os;
import numpy as np;
import numpy.random as rnd;
import sklearn.cluster as sk;
import sklearn.mixture as mx;
from copy import deepcopy;
from crp import crp_cluster;

clust_path = "/home/nburghoorn/userData/rrg/randomreadgenerator/clusterpipes/";
form = "hist";

#simulate gaussian peak data
N = 1000;
mu = [.1,.5,.9];
sigma = .1;
x = rnd.normal(mu[0], sigma, N);
y = rnd.normal(mu[1], sigma, N);
z = rnd.normal(mu[2], sigma, N);

data = list(x)+list(y)+list(z);

#write data to tmp_plot.txt
with open(clust_path + "tmp_plot.txt",'w') as f:
  for e in data[:-1]:
    f.write(str(e)+'\n');
  f.write(str(data[-1]));

vector = np.array(data).reshape(-1,1);

#kmeans
k_number = 3
centroids = list(sk.k_means(vector,k_number)[0]);
k_centers = []
centroids = [k_centers.extend(list(centroids[i])) for i in range(k_number)];
k_centers.sort();

k_error = list(abs(np.array(mu)-np.array(k_centers)));

#chinese restaurant process
crp = crp_cluster(data,0.001);

crp.initial_assignment(all_own_cluster=False,number_of_clusters = 15);
crp.smart_cluster(5,1);
c_centers = crp.m;
c_centers.sort();

#gaussian mixture model
gmm = mx.GaussianMixture(n_components = 3);
gmm.fit(vector);
gmm1mean = gmm.means_;
gmm2 = mx.GaussianMixture(n_components = 4);
gmm2.fit(vector);
gmm2mean = gmm2.means_;
gmm3 = mx.GaussianMixture(n_components = 5);
gmm3.fit(vector);
gmm3mean = gmm3.means_;

#bayesian gaussian mixture model
bgmm = mx.BayesianGaussianMixture(n_components = 3, max_iter = 1000);
bgmm.fit(vector);
bgmm1mean = bgmm.means_;
bgmm2 = mx.BayesianGaussianMixture(n_components = 4, max_iter = 1000);
bgmm2.fit(vector);
bgmm2mean = bgmm2.means_;
bgmm3 = mx.BayesianGaussianMixture(n_components = 5, max_iter = 1000);
bgmm3.fit(vector);
bgmm3mean = bgmm3.means_;

#check for equality and extract the most probable number of components
gms = [gmm1mean,gmm2mean,gmm3mean];
bms = [bgmm1mean,bgmm2mean,bgmm3mean];

ms = gms+bms;

print(ms)

#check if any element is close to other element then remove
means_left = [];
for g3 in gmm3mean:
  found = 0;
  for g1 in gmm1mean:
    if abs(g1 - g3) < 0.02 and found == 0:
      means_left.append(g1);
      found = 1;
  if found == 0:
    means_left.append(g3);

print(means_left)

def WriteLists(f,names,lists):
  for l in range(len(lists)):
    f.write("%20s" % names[l] + '\t');
    
    if isinstance(lists[l],list):
      for e in lists[l][:-1]:
        f.write("%10s" % str(round(e,8)) + '\t');
      f.write("%10s" % str(round(lists[l][-1],8)) + '\n');
    else:
      f.write("%10s" % str(round(lists[l],8)) + '\n');
      

#write center info and cluster info to tmp_centers.txt
names = ["sigma","mu","k-mean_center","crp_center"];
with open(clust_path + "tmp_centers.txt",'w') as f:
  WriteLists(f,names,[sigma,mu,k_centers,c_centers]);

#plot
os.system("Rscript " + clust_path + "Rplot_" + form + ".R");
