from random import random;
from math import sqrt;
from scipy.stats import binned_statistic; #for smart_initialize
from collections import Counter; #for smart_initialize
from copy import deepcopy;

DEBUG = 0;
def dbg(*args,**kwargs):
  if DEBUG:
    print(*args,**kwargs);

class crp_cluster():
  """
  A cluster tool using the Chinese Restaurant Process
  x = data
  a = alpha
  """
  z = []; #cluster assignments of the datapoints (1 per datapoint) - k of i
  m = []; #centers of the of clusters (1 per cluster) - m of k
  n = []; #indices of points in cluster (1 or more per cluster) - i's of k
  G = None; #base distribution
  l = None; #lenght of the dataset
  s = .1; #sigma of clusters
  
  def __init__(self,x, a = 1):
    self.x = x;
    self.l = len(x);
    self.a = a;
    self.z = [None]*self.l; #unassigned
  
  def set_base_distribution(self,G="normal"):
    self.G = G;
    
  def distribution(self):
    pass;
  
  def distribution_distance(self,m,s,x):
    dis = abs(m-x);
    
    #for normal distribution
    levels = [1.0, 0.8825, 0.3247, 0.0111];
    dd = levels[min(int(dis/self.s),3)];
    
    return(dd);
  
  def assign(self,i,new_k):
    '''Assign a point to a new cluster and remove it from the old one'''
    dbg("unassign i",i," and reassign to k",new_k);
    
    #remove from old cluster
    old_k = self.z[i];
    if old_k != None:
      self.n[old_k].remove(i);
    
    #add to new cluster (new_k - 1 if a cluster was removed)
    self.z[i] = new_k;
    self.n[new_k].append(i)

  def make_cluster(self,i):
    '''Make new cluster based on data point x[i]
    , returns the cluster number (k)'''
    dbg("make cluster based on i",i);
    
    #make new cluster
    self.n.append([]);
    self.m.append(self.x[i]);
    
    #change cluster assignment of point i
    new_k = len(self.m)-1;
    self.assign(i,new_k);
    
  def remove_cluster(self,k):
    '''Remove cluster k'''
    dbg("remove cluster k",k);
    dbg(self.n);
    del self.n[k];
    del self.m[k];
    dbg(self.n);
    #reduce assignmente of all points with "above k cluster"
    for i in range(len(self.x)):
      if self.z[i] > k:
        self.z[i] -= 1;
    
  def initial_assignment(self,all_own_cluster = True,number_of_clusters = 5, centers = None):
    '''The initial cluster assignment of the data points'''
    dbg("initial assignment")
    
    if all_own_cluster:
      for i in range(self.l):
        self.make_cluster(i);
        
    elif centers != None:
      #add centers manually (find points within 0.001 range)
      center_points = [];
      i=0;
      c=0;
      while c < len(centers):
        if abs(self.x[i] - centers[c]) < .01:
          self.make_cluster(i);
          center_points.append(i);
          c += 1;
        i+=1;
      
      #assign points to closest clusters (closest center)
      for i in range(self.l):
        if i not in center_points:
          closest_k = min(enumerate(centers), key=lambda c : self.x[i]-c[1])[0];
          self.assign(i,closest_k);
            
    else:
      
      for i in range(number_of_clusters):
        self.make_cluster(i);
      for i in range(number_of_clusters,self.l):
        self.assign(i,i%number_of_clusters);
  
  def smart_initial_assignment(self,number_of_bins = 6, all_bins = True,number_of_clusters = 2):
    '''The initial cluster assigment based on bins (with biggest size)'''
    bin_means, bin_edges, binnumber = binned_statistic(self.x,self.x,bins=number_of_bins);
    
    clusters_made = set();
    clusters_points = [];
    i = 0;
    while len(clusters_made) < number_of_bins:
      if binnumber[i] not in clusters_made:
        self.make_cluster(i);
        clusters_made.add(binnumber[i]);
        clusters_points.append(i);
      i += 1;
        
    for i in range(self.l):
      if i not in clusters_points:
        self.assign(i,binnumber[i]-1);
    
    return "";
    
    binsize = Counter(binnumber)
    selected_bins = binsize.most_common(number_of_clusters);
    centers = [bin_means[selected_bins[i][0]-1] for i in range(number_of_clusters)]
    print(bin_means, "\n", binnumber , "\n", binsize, "\n", centers);
    print(selected_bins);#TODO
    
  def iterate(self):
    for i in range(self.l):
      self.update_point(i);
    pass;
    
  def update_point(self,i):
    '''For data point x[i] possibly update its cluster assignment based on
    distance to cluster and chance (and base distribution)'''
    dbg("update i",i);
    
    #loop until joined some cluster TODO: shuffle the cluster order
    joined = False;
    while not joined:
      for k in range(len(self.m)):
        #chance that the point will go in k'th cluster, based on distance TODO: make dependent on base distribution
        pjoin = len(self.n[k])/(self.l + self.a) * self.distribution_distance(self.m[k], self.s, self.x[i]);
        
        #join k'th cluster if lucky
        if random() < pjoin:
          if self.z[i] == k:
            #already in this cluster
            pass;
          else:
            #assign x[i] to cluster k
            self.assign(i,k);
          joined = True;
          break;
      else:
        #chance to make a new cluster
        pnew = self.a/(self.l + self.a)# * ()
        
        #make new cluster if lucky
        if random() < pnew:
          if self.m[self.z[i]] == self.x[i] and self.n[self.z[i]] == [i]:
            #already its own cluster
            joined = True;
            break;
          else:
            #make new cluster
            self.make_cluster(i);
            
            joined = True;
            break;
        
    pass;

  def recalculate_clusters(self):
    '''Recalculate the centers of the clusters'''
    dbg("recalculate cluster centers")
    
    #check if old is empty -> remove cluster
    k = 0;
    clusters = len(self.m);
    while k < clusters:
      if len(self.n[k])==0:
        self.remove_cluster(k);
        k -= 1;
        clusters -= 1;
      k += 1;
    
    #for every cluster
    for k in range(len(self.n)):
      #sum
      sumk = 0;
      for i in self.n[k]:
        sumk += self.x[i];
      #divide by number of members (get average)
      self.m[k] = sumk/len(self.n[k]);
  
  def get_cluster_number(self):
    return(len(self.n));
  
  def cluster(self,iter_num):
    for it in range(iter_num):
      print(it)
      self.iterate();
      self.recalculate_clusters();
  
  def smart_cluster(self,iter_num,iter_rounds):
    '''Smart clusting, here the iter_num is more an indication of the "freedom", possible reassignment iterations.
    These iterations will have for iter_rounds rounds, where only will be continued upon the last data if it is a
    significant improvement (davies-bouldin index) on the initial assignment of the round'''
    self.saved_q = 999999;
    
    for ir in range(iter_rounds):
      for it in range(iter_num):
        self.iterate();
        self.recalculate_clusters();
      
      new_q = self.get_cluster_quality();
      print(new_q)
      
      #if no improvement, reset to round initial state
      if new_q > self.saved_q:
        print("reset")
        self.z = deepcopy(self.saved_z);
        self.n = deepcopy(self.saved_n);
        self.m = deepcopy(self.saved_m);
      else:
        print("new best")
        self.save_state();
    
  def save_state(self):
    self.saved_z = deepcopy(self.z);
    self.saved_n = deepcopy(self.n);
    self.saved_m = deepcopy(self.m);
    self.saved_q = self.get_cluster_quality(); 
    
  def get_assignments(self):
    '''Create a linked list of x and z, sorted on z (cluster assingment)'''

    links = list(zip(self.z,self.x));
    links.sort();
    
    return(links)
  
  def get_cluster_stdv(self,k,big_cluster_size = 100):
    '''Get the (discrete) standard deviation of cluster (k)
    "-1" added in denominator only when the number of points in cluster bigger than big_cluster_size (100)'''
    mu = self.m[k];
    sumk = 0;
    for i in self.n[k]:
      sumk += (self.x[i] - mu)*(self.x[i] - mu);
    
    cluster_size = len(self.n[k]);
    if cluster_size > big_cluster_size:
      return(sqrt(sumk/(cluster_size-1)));
    else:
      return(sqrt(sumk/cluster_size));
  
  def get_cluster_quality(self):
    '''Get the custom cluster quality index'''
    stdvs = [];
    for k in range(len(self.m)):
      stdvs.append(self.get_cluster_stdv(k));
      
    if len(self.m) > 1:
      ddavgs = [];
      for k in range(len(self.m)):
        m = self.m[k];
        s = self.get_cluster_stdv(k);
        
        ddsum = 0;
        for i in self.n[k]:
          ddsum += self.distribution_distance(m,s,self.x[i]);
        ddavgs.append( ddsum/len(self.n[k]) );
        
      return( sum(ddavgs) );
      
    else:
      return( 999999 );
  
  def get_davies_bouldin(self):
    '''Get the davies-bouldin index'''
    stdvs = [];
    for k in range(len(self.m)):
      stdvs.append(self.get_cluster_stdv(k));
      
    R = [];
    
    #minimum size of 2
    if len(self.m) > 1:
      for k in range(len(self.m)):
        mu_k = self.m[k]
        
        R_option = [];
        for j in range(len(self.m)):
          if j == k:
            continue;
          
          mu_j = self.m[j];
          R_option.append((stdvs[k]+stdvs[j])/abs(mu_k-mu_j));
        
        R.append(max(R_option))
      
      return( sum(R)/len(self.m) );
    else:
      return( 999999 );
    
