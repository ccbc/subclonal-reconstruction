# -*- coding: utf-8 -*-
"""
Created on Mon Jun 17 16:45:55 2019

@author: niels
"""
import math
def d(a,b):
    s = 0;
    s += (a-b)**2;
    
    return math.sqrt(s);

# import matplotlib.pyplot as plt
import sys;
ds_raw = sys.stdin.read();
from copy import deepcopy as dc

#convert data
k = 3;#int(ds_raw[0].split()[0])
m = 1;#int(ds_raw[0].split()[1])

#add centers
c = [.20,.3,5];

n = [[] for i in range(k)] #points that correspond with the k'th center

data = [];
for vaf in ds_raw.split(", "):
  data.append(float(vaf));
print(data)

for i in range(len(data)):  
    min_d = 999;
    vaf = data[i];
        
    for l in range(len(c)):
        if d(vaf,c[l]) < min_d:
            min_d = d(vaf,c[l])
            min_l = l
    n[min_l].append(i)

pc = [];

while c != pc:
    pc = dc(c)
    
    for i in range(len(c)):
        c[i] = 0
        for j in range(len(n[i])):
            c[i] += data[n[i][j]]/float(len(n[i]))
                
    #reassign point to centers
    n = [[] for i in range(k)]
    
    for i in range(len(data)):  
        min_d = 999;
        vaf = data[i];
        
        for l in range(len(c)):
            if d(vaf,c[l]) < min_d:
                min_d = d(vaf,c[l])
                min_l = l
        n[min_l].append(i)   

for i in c:
    print(str.format('{0:.3f}',round(i, 4)));
 
