general = dict(
  DEBUG = 0,
  DEBUG_READS = 0,
  DEBUG_PHYLO = 0,
  PIPE = 0,
  FISH = 1,
  PATH = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/output/",
  LOG = 1,
  LOGNAME = "log",
  LOADBAR = 1,
  SEEDFILE = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/rrg/seedfiles/",
);

class settings():
  rrg = dict(
    cellN = 200, #size of cell population
      nuclN = 1000, #number of nucleotides per chromosome to monitor + tag nucleotide (in front)
    timeT = 10, #number of cell divisions
    initial_mutation_fraction = .02, #fraction of nucleotides that is initially mutated (SNPs)
    snv_mutation_fraction = 0, #fraction of nucleotides that is additionally mutated (SNVs)
    snv_selection_penalty = 0, #(should be negative if penalty --> include "-") penalty that will make it less likely to be selected in the next round
    snv_canbe_driver = 0, #this determines if a snv can be a driver mutation
    driverMutN = 0, #number of driver mutatations for nucleotide set
    driver_selection_reward = 0, #(reward for sum all driver mutations) reward that will make it more likely to be selected in the next round
    deterministic_bigevent = 1, #whether thxe bigevents are deterministic or not
    bigevent_with_driver = 0, #whether or not to include a drivermutation into the big events
      bigevent_snv_fraction = .05, #the percentage of the read to gain additional SNVs at a big event 
      bigevent_times = [2,4,6], #AKA: Clonal expansions, if deterministic: times of after which big events take place 
    bigevent_parents = [], #the phylogenetic relation between the subclones (empty is random)
    bigevent_chance = 0, #if non-deterministic: per cell per time (only one cell can have big event)
      bigevent_percentage = .4, #if non-deterministic: portion of cell population to replace
      bigevent_percentage_stdv = .05, #if non-deterministic: standard deviation of the percentage (if zero -> fixed percentage)
      timepointN = 1, #0 - manual timeponts; (1 and up) is regularly spaced timepoints
    spatialN = 0, #0 (default) - whole population is a sample; (1 and up) is samples of population (blocks of final population) LEAVE "timepointsN=0" AND "timepoints=[]"!
    spatial_fraction = 0, # the fraction of the whole population to take as a sample (block of final population)
    timepoints = [], #time points after which the measurements take place and reads are retagged
      seq_noise = .05, #the noise imposed on the VAFs to broaden the peaks (simulate the sequencing)
      mcmc_samples = 25, #the number of mcmc samples (there are "mcmc_sample//2" burn-in samples additionally)
  );
  
  #for stats runs (these are selected if not specifically indicated in run)
  default_rrg = dict(
    nuclN = 1000, #number of nucleotides per chromosome to monitor + tag nucleotide (in front)
    bigevent_snv_fraction = .05, #the percentage of the read to gain additional SNVs at a big event 
    bigevent_times = [2,4,6], #AKA: Clonal expansions, if deterministic: times of after which big events take place 
    bigevent_percentage = .4, #if non-deterministic: portion of cell population to replace
    bigevent_percentage_stdv = .05, #if non-deterministic: standard deviation of the percentage (if zero -> fixed percentage)
    timepointN = 1, #0 - manual timeponts, (1 and up) is regularly spaced timepoints
    seq_noise = .05, #the noise imposed on the VAFs to broaden the peaks (simulate the sequencing)
    mcmc_samples = 25, #the number of mcmc samples (there are "mcmc_sample//2" burn-in samples additionally)
  );
