'''Average the reads from the population and get the CCFs per locus'''
import sys;
from random import seed,gauss;
from settings import settings;

SEQ_NOISE = settings.rrg["seq_noise"];

ds_raw = sys.stdin.read();

data_lines = ds_raw.split("\n")[1:-1];
readN = len(data_lines);

if len(data_lines) == 0:
  sys.exit("ERROR: No input (no mutations in datafiles)");

#initiate vafs with first read values (zeros and ones)
vafs = [];
for val in data_lines[0].split(","):
  vafs.append(int(val));

#add corresponding values for every other read (zeros and ones)
for line in data_lines[1:-1]:
  vals = line.split(",");
  for val in range(len(vals)):
    vafs[val] += int(vals[val]);

#divide by the number of reads to get actual VAF of every locus
for vaf in range(len(vafs)):
  vafs[vaf] = vafs[vaf]/float(readN);
  
#remove zeros and ones if indicated
if len(sys.argv) < 2:
  assert("Error: specify if edge values are kept (0 and 1 VAFs)")
if sys.argv[1] == "noedge":
  import numpy as np;
  np_vafs = np.array(vafs);
  np_vafs = np_vafs[np.where(np_vafs>0)[0]]; #impose lower bound
  np_vafs = np_vafs[np.where(np_vafs<1)[0]]; #impose upper bound
  vafs = np_vafs.tolist();
if sys.argv[1] == "nozero":
  import numpy as np;
  np_vafs = np.array(vafs);
  np_vafs = np_vafs[np.where(np_vafs>0)[0]]; #impose lower bound
  vafs = np_vafs.tolist();

#use seed if indicated
if len(sys.argv) == 3:
  seed(int(sys.argv[2]));

# introduce normal error (sequencing noise/error)
for vaf in range(len(vafs)):
  vafs[vaf] = max(vafs[vaf] + gauss(0,SEQ_NOISE),0);

#if empty vafs add a zero
if len(vafs) == 0:
  sys.exit("ERROR: No CCFs found");
else:
  for vaf in vafs[:-1]:
    print("%.9f" % vaf,end=", ");
  print("%.9f" % vafs[-1])
