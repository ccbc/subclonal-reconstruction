rrg_path=/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/
out_path=/mnt/data/ccbc_environment/users/nburghoorn/rrg/stats_data/

rm -Rf ${out_path}$1
mkdir ${out_path}$1
mv ${rrg_path}output/visual_report.pdf ${rrg_path}output/pipe_estimations.npy ${rrg_path}output/true_values.npy ${rrg_path}stats_files/log.txt ${out_path}$1
cp ${rrg_path}settings.py ${out_path}$1/settings.txt