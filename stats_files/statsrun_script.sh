rrg_path=/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/

stats_run () {
  #$1 : command
  #$2 : export name
  
  if $1 > ${rrg_path}stats_files/log.txt; then
    echo "stats_run succesfull, exporting info..."
    ${rrg_path}stats_files/statsrun_export.sh $2
  else
    echo "stats_run failed"
  fi
}

stats_run \
"python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 30 -l 6 -m 0.001:4:0.301 -n 1 -x 25" \
SAMPLE_MCMC_s1

stats_run \
"python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 30 -l 6 -m 0.001:4:0.301 -n 2 -x 50" \
SAMPLE_MCMC_s2

stats_run \
"python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 30 -l 6 -m 0.001:4:0.301 -n 3 -x 75" \
SAMPLE_MCMC_s3

stats_run \
"python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 30 -l 6 -m 0.001:4:0.301 -n 4 -x 100" \
SAMPLE_MCMC_s4

# TODO: ~24h
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 2" \
# PWGSFIX_4_s3_0
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 3" \
# PWGSFIX_4_s4_0
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 2" \
# PWGSFIX_4_s3_1
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 3" \
# PWGSFIX_4_s4_1

# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 0" \
# PWGSFIX_4_s1_2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 1" \
# PWGSFIX_4_s2_2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 0" \
# PWGSFIX_4_s1_3
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.001:6:0.301 -n 1" \
# PWGSFIX_4_s2_3

#Phyfix
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 40 -l 6 -m 0.001:19:0.01 -n 0" \
# PWGSFIX_s1_1
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 40 -l 6 -m 0.001:19:0.01 -n 1" \
# PWGSFIX_s2_1
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 40 -l 6 -m 0.001:19:0.01 -n 0" \
# PWGSFIX_s1_2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 40 -l 6 -m 0.001:19:0.01 -n 1" \
# PWGSFIX_s2_2

#TODO
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_1_samples_CCFfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_2_samples_CCFfix2


#CCfix mutload datasets 4 subclones
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_1_samples_1r4sc_CCFfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_2_samples_1r4sc_CCFfix2

#TODO
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 2 -s 20 -l 6 -m 0.002:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_1_samples_CCFfix2_sc4
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 2 -s 20 -l 6 -m 0.002:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_2_samples_CCFfix2_sc4

#CCfix mutload datasets 1 2 3 4
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_1_samples_CCFfix3
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_2_samples_CCFfix3
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_1_samples_CCFfix4
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_0002_20_02m_-_1000g_2_samples_CCFfix4

#CCFfix 4 sample - genome sizes , general dataset
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 0" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_1_samples_CCFfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 1" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_2_samples_CCFfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 2" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_3_samples_CCFfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 3" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_4_samples_CCFfix


# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:5:0.05 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_002_5_005m_-_2000g_1_samples_ISAfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:5:0.05 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_002_5_005m_-_2000g_2_samples_ISAfix

# TODO:
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:5:0.05 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_002_5_005m_-_2000g_1_samples_ISAfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.002:5:0.05 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_002_5_005m_-_2000g_2_samples_ISAfix2

#Sciclone search
# stats_run \
# "python3 ${rrg_path}ui.py -p sci -r 1 -s 20 -l 6 -m 0.002:8:0.2 -n 1" \
# sci_20s_6l_search1
# stats_run \
# "python3 ${rrg_path}ui.py -p sci -r 1 -s 20 -l 6 -m 0.01 -n 0:6:5" \
# sci_20s_6l_search2
# stats_run \
# "python3 ${rrg_path}ui.py -p sci -r 1 -s 20 -l 6 -m 0.01 -q 0.01:8:0.1" \
# sci_20s_6l_search3


# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.01:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_01_20_2m_-_2000g_1_samples_ISAfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.01:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_01_20_2m_-_2000g_2_samples_ISAfix
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.01:20:0.2 -n 0" \
# pwgs-dpc_it25_20s_6l_mutload_01_20_2m_-_2000g_1_samples_ISAfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -m 0.01:20:0.2 -n 1" \
# pwgs-dpc_it25_20s_6l_mutload_01_20_2m_-_2000g_2_samples_ISAfix2

# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 0" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_1_samples_ISAfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 1" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_2_samples_ISAfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 2" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_3_samples_ISAfix2
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 20 -l 6 -g 500:10:5000 -n 3" \
# pwgs-dpc_it25_20s_6l_500_10_5000g_-_4_samples_ISAfix2

# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,sci,dpc -r 1 -s 50 -l 6 -g 500 -n 0:4:3" \
# pwgs-sci-dpc_it25_50s_6l_500g_0_4_4n_mutfix
# 
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,sci,dpc -r 1 -s 50 -l 6 -g 1000 -n 0:4:3" \
# pwgs-sci-dpc_it25_50s_6l_1000g_0_4_4n_mutfix
# 
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,sci,dpc -r 1 -s 50 -l 6 -g 2000 -n 0:4:3" \
# pwgs-sci-dpc_it25_50s_6l_2000g_0_4_4n_mutfix


# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,sci,dpc -r 1 -s 30 -l 6 -n 0:4:3" \
# pwgs-sci-dpc_it25_20s_6l_0_4_3n

# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 50 -l 6 -g 500:7:5000 -n 0" \
# pwgs-sci-dpc_it25_20s_6l_500_10_5000g_-_1_samples_double_initial_muts
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 50 -l 6 -g 500:7:5000" \
# pwgs-sci-dpc_it25_20s_6l_500_10_5000g_-_2_samples_double_initial_muts
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs,dpc -r 1 -s 50 -l 6 -g 500:7:5000" \
# pwgs-sci-dpc_it25_20s_6l_500_10_5000g_-_2_samples_double_initial_muts

# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs -m 0.001:20:0.020 -l 10 -s 10" \
# mutload_pwgs_small
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs -c 0.2:20:0.6 -l 10 -s 10" \
# sc_size_pwgs_small
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs -m 0.001:20:0.020 -l 10 -s 20" \
# mutload_pwgs_big
# 
# stats_run \
# "python3 ${rrg_path}ui.py -p pwgs -c 0.2:20:0.6 -l 10 -s 20" \
# sc_size_pwgs_big