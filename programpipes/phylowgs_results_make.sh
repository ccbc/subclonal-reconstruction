#Writing the results THIS IS FILE OUTSIDE PHYLOFOLDER --> COPY INTO PHYLOWGS FOLDER

#make dir (name is first argument) this file should be in phylowgs folder
mkdir $1
#run write_results using trees.zip data
/usr/bin/python2 write_results.py $1 /mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs/chains/trees.zip $1.summ.json.gz $1.muts.json.gz $1.mutass.zip
#move all files with given name in phylowgs folder to created directory (intermediate results moved into folder)
mv ./$1.* $1
#if there is a folder with same name in witness/data, delete it
rm -Rf /mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs/witness/data/$1
#move created directory with intermediate results into witness/data
mv $1 /mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs/witness/data
#move into witness directory
cd /mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs/witness/
#unpack intermediate data
gunzip data/$1/*.gz
#process unpacked intermediate data into index object
python2 index_data.py