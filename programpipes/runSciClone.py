import sys;
import os;
import getPaths;

#get all paths
rrg_path = getPaths.paths["rrg"];
sample_path = rrg_path + "output/";

#if process_id is indicated, run multiple of the sciclone in parallel
if len(sys.argv) == 2:
  process_id = int(sys.argv[1]);
  sample_path = rrg_path + "output/process_" + str(process_id) + "/";
else:
  process_id = None;

#get file names of data files
os.chdir(sample_path);
data_file_prefix = "reads_data"
sample_files = [f for f in os.listdir(".") if f[:len(data_file_prefix)] == data_file_prefix];
sample_tags = [0 if f[-5:]=="a.txt" else int(f[-5]) for f in sample_files];
with open("reads_data.txt") as f:
  sample_readN = list(enumerate(f))[-1][0];
  
#sort them
sample_files = [x for _,x in sorted(zip(sample_tags,sample_files))];
sample_files.append(sample_files.pop(0));

#extract ccfs from files using "ReadsToVAF.py"
os.chdir(sample_path);
all_ccfs = [];
for sample_fileID in range(len(sample_files)):
  os.system("cat " + sample_files[sample_fileID] + " | python3 " + rrg_path + "ReadsToCCFs.py nozero > tmp_sci.txt");
  with open("tmp_sci.txt","r") as tmp_file:
    line = tmp_file.readlines()[0];
    ccfs = [];
    for ccf in line.split(","):
      ccfs.append(ccf);
    #remove "\n"
    ccfs[-1] = ccfs[-1][:-2];
    
  all_ccfs.append(ccfs);
  
  #delete tmp_sci.txt files
  os.system("rm -f ./tmp_sci.txt");
  
def SetSciValues(readN,ccf,wt,mut,vafs):
  #get reads that are mutated
  mut_reads = int(readN * float(ccf)/2);
  
  #wt
  wt.append( readN - mut_reads );
  
  #mut
  mut.append(mut_reads);
  
  #vafs (as percentages)
  vafs.append(float(ccf) * 50);

#lists for all samples
all_wt = [];
all_mut = [];
all_vafs = [];

#Extract SciClone-relavent info from calls:
for sampleID in range(len(sample_files)):
  wt = [];
  mut = [];
  vafs = [];
  

  for ccfID in range(len(all_ccfs[sampleID])):
    SetSciValues(sample_readN,
                  all_ccfs[sampleID][ccfID],
                  wt,
                  mut,
                  vafs)
  
  all_wt.append(wt);
  all_mut.append(mut);
  all_vafs.append(vafs);

#make a list with string starting from s increasing by c every step
def MakeListWith(s,c=0):
  return([[str(s+i*c) for i in range(len(all_ccfs[sampleID]))] for sampleID in range(len(sample_files))]);

all_chrm = MakeListWith(1);
all_pos = MakeListWith(1,1);
#wt
#mut
#vafs

#remove the VAF that are to low to speed up analysis TODO: make this a global parameter (for all pipelines)
CUTOFF = 0.05 #5% cutoff (lower than 5% of reads is mutated -> VAF not considered), this speeds up the analysis considerably (less datapoints)
vaf_loci = list(zip( *all_wt ));
loci_sums = list(map(sum,vaf_loci));

excluded_loci = [];
for locus_ID in reversed(range(len(loci_sums))):
  # print(loci_sums[locus_ID],"compared to",(sample_readN*(1-CUTOFF))*len(sample_files));
  if loci_sums[locus_ID] >= (sample_readN*(1-CUTOFF))*len(sample_files):
    excluded_loci.append(locus_ID);
    # print("add");

#make df
df = list(list(zip(all_chrm,all_pos))[0]);

#merge df with each of the sample columns
for sampleID in range(len(sample_files)):
  col_triplet = list(zip(all_wt[sampleID],all_mut[sampleID],all_vafs[sampleID]))
  col_triplet = list(zip(*[map(str,col) for col in col_triplet]));
  for i in range(3):
    df.append(col_triplet[i]);

print(excluded_loci)

# for locus_ID in excluded_loci:
#     del df[locus_ID];

#transpose (rows are lines now)
df = list(zip(*df));

print(len(df));

#make header for sci file
if len(sample_files) == 1:
  sample_headers = ["ref_count","var_count","vaf"];
else:
  sample_headers = [["ref_count_"+str(i),"var_count_"+str(i),"vaf_"+str(i)] for i in range(len(sample_files))];
  sample_headers = [col_name for sample_header in sample_headers for col_name in sample_header];
header = "\t".join(["chr","pos"] + sample_headers);

#get filename
filename = sample_path + "tmp_sci_input.txt";

#write to file
with open(filename,'w') as file:
  #write header
  file.write(header+"\n");

  #write data file rows
  for df_row in df[:-1]:
      # print("df_row:\n",df_row);
      file.write('\t'.join(df_row));
      file.write('\n');
  file.write('\t'.join(df[-1]));

# =============================================================================
# execute SciClone, and save output in specified sample folder
# =============================================================================

#make sure sample folder is available and empty
##this is done inside the "sciclone_pipeline.R"

if process_id == None:
  process_tag = "";
else:
  process_tag = " " + str(process_id);

#execute dpclust_pipeline locally
os.chdir(rrg_path + "programpipes/")
run_string = "Rscript sciclone_pipeline.R " + str(len(sample_files)) + process_tag;
os.system(run_string);

#remove all temporary text files
os.chdir(sample_path);

os.system("rm -f  ./tmp_sci*.txt");
