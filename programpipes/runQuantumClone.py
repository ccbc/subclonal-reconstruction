import sys;
import os;
import getPaths;

#get all paths
rrg_path = getPaths.paths["rrg"];
quc_path = getPaths.paths["quantumclone"];
sample_path = rrg_path + "output/";

#if process_id is indicated, run multiple of the quantumclone_pipelines in parallel
if len(sys.argv) == 2:
  process_id = int(sys.argv[1]);
  sample_path = rrg_path + "output/process_" + str(process_id) + "/";
else:
  process_id = None;

#get file names of data files
os.chdir(sample_path);
data_file_prefix = "reads_data"
sample_files = [f for f in os.listdir(".") if f[:len(data_file_prefix)] == data_file_prefix];
sample_tags = [0 if f[-5:]=="a.txt" else int(f[-5]) for f in sample_files];
with open("reads_data.txt") as f:
  sample_readN = list(enumerate(f))[-1][0];
  
#sort them
sample_files = [x for _,x in sorted(zip(sample_tags,sample_files))];
sample_files.append(sample_files.pop(0));

#extract vafs from files using "ReadsToVAF.py"
os.chdir(sample_path);
all_vafs = [];
for sample_fileID in range(len(sample_files)):
  os.system("cat " + sample_files[sample_fileID] + " | python3 " + rrg_path + "ReadsToCCFs.py nozero > tmp_quc.txt");
  with open("tmp_quc.txt","r") as tmp_file:
    line = tmp_file.readlines()[0];
    vafs = [];
    for vaf in line.split(","):
      vafs.append(vaf);
    #remove "\n"
    vafs[-1] = vafs[-1][:-2];
  all_vafs.append(vafs);
  
  #delete tmp_quc.txt files
  os.system("rm -f ./tmp_quc.txt");
  
def SetQUCValues(readN,vafID,vaf,depth,alt):
  #get reads that are mutated
  mut_reads = int(readN * float(vaf)/2);
  
  #wt
  depth.append( readN );
  
  #mut
  alt.append(mut_reads);
  
#lists for all samples
all_depth = [];
all_alt = [];

#Extract QuantumClone-relavent info from calls:
for sampleID in range(len(sample_files)):
  depth = [];
  alt = [];

  for vafID in range(len(all_vafs[sampleID])):
    SetQUCValues(sample_readN,
                  vafID,
                  all_vafs[sampleID][vafID],
                  depth,
                  alt,
                  )
  
  all_depth.append(depth);
  all_alt.append(alt);

def MakeListWith(s,c=0):
  return([[str(s+i*c) for i in range(len(all_vafs[sampleID]))] for sampleID in range(len(sample_files))]);

all_sampleset = [["Sampleset" for i in range(len(all_vafs[sampleID]))] for sampleID in range(len(sample_files))]
all_sample = [["Sample_"+str(sampleID) for i in range(len(all_vafs[sampleID]))] for sampleID in range(len(sample_files))]
all_chrm = MakeListWith(1);
all_pos = MakeListWith(1,1);
#depth
#alt
all_genotype = [["AB"]*len(all_vafs[sampleID])]*len(sample_files);

#remove the VAF that are to low to speed up analysis
CUTOFF = 0.05 #5% cutoff (lower than 5% of reads is mutated -> VAF not considered), this speeds up the analysis considerably (less datapoints)
vaf_loci = list(zip( *all_alt ));
loci_sums = list(map(sum,vaf_loci));

#remove all calls with less than or equal to 5% variant reads
excluded_loci = [];
for locus_ID in reversed(range(len(loci_sums))):
  if loci_sums[locus_ID] <= (sample_readN*CUTOFF)*len(sample_files):
    excluded_loci.append(locus_ID);

#make df
df = []; #dataframe per sample
for sampleID in range(len(sample_files)):
  sample_df = list(zip(all_sampleset[0],all_sample[sampleID],all_chrm[0],all_pos[0],
                   list(map(str,all_depth[sampleID])),list(map(str,all_alt[sampleID])),
                   all_genotype[0]));
  
  for locus_ID in excluded_loci:
    del sample_df[locus_ID];
  
  df.append(sample_df); 

#make header for quc file
header = "\t".join(["Sample", "SampleName", "Chr", "Start", "Depth", "Alt", "Genotype"]);
# print(header)

#make input file for each sample
filenames = [];
for sampleID in range(len(sample_files)):
  #get filename
  filename = "tmp_quc_sample_" + str(sampleID) + ".txt";
  filenames.append(filename);
  
  #write to file
  with open(sample_path + filenames[-1],'w') as file:
    #write header
    file.write(header+"\n");
  
    #write data file rows
    for df_row in df[sampleID][:-1]:
        # print("df_row:\n",df_row);
        file.write('\t'.join(df_row));
        file.write('\n');
    file.write('\t'.join(df[sampleID][-1]));

# =============================================================================
# execute QuantumClone, and save output in output folder
# =============================================================================

#make sure output folder is available and empty
if not os.path.exists(sample_path + "quantumclone_output"):
  os.mkdir(sample_path + "quantumclone_output")
os.system("rm -Rf ./quantumclone_output/*");

if process_id == None:
  process_tag = "";
else:
  process_tag = " " + str(process_id);

#execute quantumclone_pipeline locally
os.chdir(rrg_path + "programpipes/")
run_string = "Rscript quantumclone_pipeline.R " + str(len(sample_files)) + " " + process_tag;
os.system(run_string);

#remove all temporary text files
os.chdir(sample_path);

os.system("rm -f  ./tmp_quc*.txt");
