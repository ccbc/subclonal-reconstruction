#this is a the sciclone pipeline
#TODO: minimumDepth parameter

library(sciClone)
sample_path = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/output/"

args = commandArgs(trailingOnly = TRUE);

if (length(args) == 2){
  sample_path = paste0(sample_path,"process_",strtoi(args[2]),"/");
  sample_number = strtoi(args[1]);
} else if (length(args) == 1) {
  sample_number = strtoi(args[1]);
} else {
  sample_number = 1;
}

#read in vaf data
v = read.table(paste0(sample_path,"tmp_sci_input.txt"),header=T);
vafs <- c();
for (i in seq(sample_number)){
  vafs[[i]] <- v[,c(1,2,(i*3),(i*3)+1,(i*3)+2)];
}

#set sample names
names = paste0("Sample",seq(0,sample_number-1))

#make an output directory, deleting old results first if they exist
suppressWarnings(dir.create(paste0(sample_path,"sciclone_output")))
unlink(paste0(sample_path,"sciclone_output/*"), recursive=TRUE)


cat("\n")
cat("=========================================================\n")
cat(paste0("Run Sciclone for ",sample_number," samples"));
cat("\n")

# numClusts = max(10,as.integer(length(vafs[[1]]$chr)/2));
# print(paste0("Clusters initially: ",numClusts));

#run one sample
sc = sciClone(vafs=vafs,
              sampleNames=names,
              minimumDepth = 1,
              maximumClusters=10#numClusts,
              )
# writeClusterTable(sc, paste0(sample_path,"sciclone_output/clusters1"));

# sc.plot1d(sc, paste0(sample_path,"sciclone_output/clusters1.1d.pdf"))

ccfs <- attributes(sc)$clust$cluster.means
write.table(ccfs, file = paste0(sample_path,"sciclone_output/sciclone_estimated_ccfs.txt"),
            row.names = F, col.names = F, sep = ",")
