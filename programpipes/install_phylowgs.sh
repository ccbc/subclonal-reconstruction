#Install+Compile the PhyloWGS program and give it easy result write script
MYDIR=/mnt/data/ccbc_environment/users/nburghoorn/

cd ${MYDIR}software/
rm -Rf ./phylowgs
git clone https://github.com/morrislab/phylowgs.git
cd ./phylowgs
#git checkout 12e7a79 (older version with proper visualization)
g++ -o mh.o -O3 mh.cpp  util.cpp `gsl-config --cflags --libs`
cp ${MYDIR}rrg/randomreadgenerator/programpipes/phylowgs_results_make.sh ./niels_make.sh
