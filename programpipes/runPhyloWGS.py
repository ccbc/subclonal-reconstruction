import sys;
import os;
import getPaths;

if len(sys.argv) > 2:
  mcmc_samples = int(sys.argv[2]);
else:
  mcmc_samples = 25;

#get all paths
rrg_path = getPaths.paths["rrg"];
pwgs_path = getPaths.paths["phylowgs"];

#if process_id is indicated, run in parallel (horizontally scaled phylowgs)
if len(sys.argv) > 1:
  process_id = int(sys.argv[1]);
  output_path = rrg_path + "output/process_" + str(process_id) + "/";
  pwgs_path = getPaths.paths["phylowgs_parallel"] + str(process_id);
else:
  process_id = None;
  output_path = rrg_path + "output/";

#get file names of data files
os.chdir(output_path);
data_file_prefix = "reads_data"
sample_files = [f for f in os.listdir(".") if f[:len(data_file_prefix)] == data_file_prefix];
sample_tags = [0 if f[-5:]=="a.txt" else int(f[-5]) for f in sample_files];
with open("reads_data.txt") as f:
  sample_readN = list(enumerate(f))[-1][0];

#sort them
sample_files = [x for _,x in sorted(zip(sample_tags,sample_files))];
sample_files.append(sample_files.pop(0));

#extract vafs from files using "ReadsToVAF.py"
os.chdir(rrg_path);
all_vafs = [];

for sample_fileID in range(len(sample_files)):
  os.system("cat " + output_path + sample_files[sample_fileID] + " | python3 ReadsToCCFs.py nozero > " + output_path + "tmp_pwgs.txt");
  with open(output_path + "tmp_pwgs.txt","r") as tmp_file:
    line = tmp_file.readlines()[0];
    vafs = [];
    for vaf in line.split(","):
      vafs.append(vaf);
    #remove "\n"
    vafs[-1] = vafs[-1][:-2];
  all_vafs.append(vafs);
  
  #delete tmp_tc.txt file
  os.system("rm -f " + output_path + "/tmp_pwgs.txt");

def SetPWGSValues(readN,vafID,vaf,ID,gene,a,d,mu_r,mu_v):
  #ID
  ID.append('s'+str(vafID));
  
  #GENE
  gene.append('S'+str(vafID));
  
  #a
  a.append( int(readN) - int(readN*float(vaf)/2) );
  
  #d
  d.append(str(int(readN)));
  
  #mu_r
  mu_r.append('1');
  
  #mu_v
  mu_v.append('0.5');

#lists for all samples
all_ID = [];
all_gene = [];
all_a = [];
all_d = [];
all_mu_r = [];
all_mu_v = [];

#Extract phyloWGS-relavent info from calls:
for sampleID in range(len(sample_files)):
  ID = [];
  gene = [];
  a = [];
  d = [];
  mu_r = [];
  mu_v = [];

  for vafID in range(len(all_vafs[sampleID])):
    SetPWGSValues(sample_readN,
                  vafID,
                  all_vafs[sampleID][vafID],
                  ID,
                  gene,
                  a,
                  d,
                  mu_r,
                  mu_v)
  
  all_ID.append(ID);
  all_gene.append(gene);
  all_a.append(a);
  all_d.append(d);
  all_mu_r.append(mu_r);
  all_mu_v.append(mu_v);

def makelist(a):
  return ','.join(list(map(str,a)));

a_zip = list( map(makelist,list(zip(*all_a))) );
d_zip = list( map(makelist,list(zip(*all_d))) );


#make df
df = list(map(list,zip(all_ID[0],all_gene[0],a_zip,d_zip,all_mu_r[0],all_mu_v[0])));

#remove the VAF that are to low to speed up analysis
CUTOFF = 0.05 #5% cutoff (lower than 5% of reads is mutated -> VAF not considered), this speeds up the analysis considerably (less datapoints)
vaf_loci = list(zip( *all_a ));
loci_sums = list(map(sum,vaf_loci));

#delete low VAF rows from dataframe (reversed order so no index issues with decreasing length)
for locus_ID in reversed(range(len(loci_sums))):
  if loci_sums[locus_ID] >= (sample_readN*(1-CUTOFF))*len(sample_files):
    del df[locus_ID];

#make sure the identicators (first row are still consequtive s0,s1,s2,..)
for row in range(len(df)):
  df[row][0] = all_ID[0][row];


#make header for tc file
header = "\t".join(["id","gene","a","d","mu_r","mu_v"]);
# print(header)

#get filename
filename = output_path + "tmp_pwgs_input.txt";

#write to file
with open(filename,'w') as file:
  #write header
  file.write(header+"\n");

  #write data file rows
  for df_row in df[:-1]:
      # print("df_row:\n",df_row);
      file.write('\t'.join(df_row));
      file.write('\n');
  file.write('\t'.join(df[-1]));
  
# =============================================================================
# execute phylowgs, and save output in output folder
# =============================================================================
#make sure output folder doesn't have old data-files (remove them if necessary)
os.system("rm -Rf " + output_path + "/phylowgs_output/data/*");
#make empty file for (lack of) cnv-measurement
os.system("touch " + output_path + "tmp_pwgs_empty.txt");

os.chdir(pwgs_path);

#clear the way, remove the chains folder if there
os.system("rm -Rf ./chains");
pwgs_samples = mcmc_samples;
pwgs_burnins = pwgs_samples//mcmc_samples;
#execute phylowgs inside pwgs-folder to get easy settings
os.system("python2 multievolve.py --num-chains 1 \
          --ssms " + output_path + "tmp_pwgs_input.txt \
          --cnvs " + output_path + "tmp_pwgs_empty.txt \
          --chain-inclusion-factor 1.5 \
          --burnin-samples " + str(pwgs_burnins) + " --mcmc-samples " + str(pwgs_samples));

#make visual results
if process_id != None:
  pwgs_output_name = "niels_run_process_"+str(process_id);
  os.system("./niels_make.sh " + pwgs_output_name + " " + str(process_id));
else:
  pwgs_output_name = "niels_run";
  os.system("./niels_make.sh " + pwgs_output_name);


#copy link to visual results to the output folder
os.system("cp -r ./witness/data/index.json " + rrg_path + "output/phylowgs_output/data");
os.system("cp -r ./witness/data/" + pwgs_output_name + " " + rrg_path + "output/phylowgs_output/data");

#remove all temporary text files
os.chdir(output_path);
os.system("rm -f  ./tmp_pwgs_input.txt ./tmp_pwgs_empty.txt");
