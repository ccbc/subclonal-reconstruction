# Script to get program data: SHOULD RETURN subclone number, cancer cell fractions
import getPaths;
import glob;
import json;
import os;
import numpy as np;

rrg_path = getPaths.paths["rrg"];

def get_dpclust(process_id):
  file_path = rrg_path+"output/process_" + str(process_id) + "/dpclust_output/sampleset*/*Info.txt";
  
  file = glob.glob(file_path);
  
  if len(file) != 0:
    ccfs = [];
    with open(file[0], 'r') as f:
      lines = f.readlines();
      for line in lines[1:]:
        vals = line.split("\t");
        #sc_id,mut_n = list(map(int,[vals[0],vals[-1]])); #(TODO) use this info
        ccf =  list(map(float,vals[1:-1])); #the ccfs of one subclone
        ccfs.append(ccf);
      sc_n = len(ccfs);
  else:
    sc_n = 0;
    ccfs = [];
    
  return(sc_n,ccfs);

def get_phylowgs(process_id):
  file_path = rrg_path+"output/phylowgs_output/data/niels_run_process_" + str(process_id) + \
              "/niels_run_process_" + str(process_id) + ".summ.json";
  
  ccfs = [];
  if os.path.isfile(file_path):
    with open(file_path) as f:
      data = json.load(f);
      
      SAMPLES = len(list(data["trees"]["0"]["populations"]["0"]["cellular_prevalence"]));
      best_tree = None;
      highest_llh = -999999;
      
      ssms = [];
      llhs = [];
      for tree in data["trees"]:
        llhs.append(float(data["trees"][tree]["llh"]));
        ssms.append(0);
        for pop in data["trees"][tree]["populations"]:
          ssms[-1] += int(data["trees"][tree]["populations"][pop]["num_ssms"]);
      
      ssms = np.array(ssms);
      llhs = np.array(llhs);
      
      nlgLH = -llhs/ssms/SAMPLES/np.log(2);
      
      nlgLH = np.round(nlgLH,1);
      
      best_tree = str(np.argmin(nlgLH));
      # print(best_tree)
      # print(np.min(nlgLH));
      
      # print(data["trees"][best_tree]["populations"])
      sc_n = len(data["trees"][best_tree]["populations"])-1;
      # print(sc_n)
      for i in range(1,sc_n+1):
        ccfs.append(list(data["trees"][best_tree]["populations"][str(i)]["cellular_prevalence"]));
  else:
    sc_n = 0;
    ccfs = [];
  
  return(sc_n,ccfs);

def get_sciclone(process_id):
  file_path = rrg_path+"output/process_" + str(process_id) + "/sciclone_output/sciclone_estimated_ccfs.txt";
  
  if os.path.isfile(file_path):
    ccfs = [];
    with open(file_path, 'r') as f:
      lines = f.readlines();
      for line in lines:
        vals = line.split(',');
        ccf =  list(map(lambda x : float(x) * 2,vals)); #the ccfs of all subclones at single sampling time
        ccfs.append(ccf);
      
    #transpose data so each row contains one subclone
    ccfs = list(zip(*ccfs));
    sc_n = len(ccfs);
  else:
    ccfs = [];
    sc_n = 0;
  
  return(sc_n,ccfs);
  
def get_quantumclone(process_id):
  file_path = rrg_path+"output/process_" + str(process_id) + "/quantumclone_output/quantumclone_estimated_ccfs.txt";
  
  if os.path.isfile(file_path):
    ccfs = [];
    with open(file_path, 'r') as f:
      lines = f.readlines();
      for line in lines:
        vals = line.split(',');
        ccf =  list(map(float ,vals)); #the ccfs of all subclones at single sampling time
        ccfs.append(ccf);
      
    #each row contains one subclone in ccfs
    sc_n = len(ccfs);
  else:
    ccfs = [];
    sc_n = 0;
  
  return(sc_n,ccfs);

