paths = dict(
  rrg = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/",
  patient = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/patient_data/",
  targetclone = "/mnt/data/ccbc_environment/users/nburghoorn/UTUC_project/TargetClone/targetclone/TargetClone/",
  phylowgs = "/mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs/",
  phylowgs_parallel = "/mnt/data/ccbc_environment/users/nburghoorn/software/phylowgs_parallel/phylowgs_",
  subclonalselection = "/mnt/data/ccbc_environment/users/nburghoorn/.julia/dev/SubClonalSelection/",
  scs_output = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/output/scs_output/scs_data/finalpopulation/posterior/",
  dpclust = "/mnt/data/ccbc_environment/users/nburghoorn/software/DPClust/",
  quantumclone = "/mnt/data/ccbc_environment/users/nburghoorn/software/QuantumClone/",
);
