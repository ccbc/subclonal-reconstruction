println("This is the julia script for running SubClonalSelection")

using SubClonalSelection
using Random
Random.seed!(123)
out = fitABCmodels(ARGS[1],
  "scs_data",
  read_depth = 300,
  resultsdirectory = "../output/scs_output",
  Nmaxinf = 10^6,
  maxiterations = 5*10^3,
  save = true,
  maxclones = 2,
  nparticles = 100);

println(out);

plothistogram(out, 0);
savefig("../output/scs_output/subclone0")

plothistogram(out, 1);
savefig("../output/scs_output/subclone1")

plothistogram(out, 2);
savefig("../output/scs_output/subclone2")

plotmodelposterior(out);
savefig("../output/scs_output/modelposterior")

plotparameterposterior(out, 1);
savefig("../output/scs_output/modelposterior")