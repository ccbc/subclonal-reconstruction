import os;
import getPaths;

rrg_path = getPaths.paths["rrg"];

#get the vafs from the reads_data and put them in the scs-format using terminal commands
os.chdir(rrg_path);
os.system("cat output/" + "reads_data.txt" + " | \
           python3 ReadsToCCFs.py noedge | \
           tr ',' '\n' | \
           tr --delete ' ' | \
           awk '{printf \"%.18f\\n\",$1/2}' | \
           awk '/^ *[0-9]+\\.[0-9]+/{sub(/0+$/,\"\");sub(/\\.$/,\".0\")}1' \
           > tmp_scs.txt");
#line by line:
  #cat the reads_data.txt
  #convert to VAFs
  #replace the commas by newlines
  #remove the spaces
  #space the VAFs values up to 18 digits
  #remove trailling zeros
  #save the preproccesed data in temporary file

#execute scs with the constructed input
os.chdir(rrg_path + "programpipes/");
os.system("julia -p 1 SubClonalSelection.jl " + rrg_path + "tmp_scs.txt");

#take data and transform into plots
# os.system("Rscript scsPlot.R" + rrg_path + "output/")

#remove all temporary text files
os.chdir(rrg_path);
os.system("rm -f ./tmp_scs.txt");
