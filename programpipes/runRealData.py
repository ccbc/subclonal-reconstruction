import getPaths;
import os;
import sys;

rrg_path = getPaths.paths["rrg"];
dpc_path = getPaths.paths["dpclust"];
pwgs_path = getPaths.paths["phylowgs"];
patient_path = getPaths.paths["patient"];
sample_path = patient_path + "output/";

patient_id = int(sys.argv[1]);
os.system("Rscript " + patient_path + "patientDataProcessing.R " + str(patient_id))

considered_pat = "patient"+str(patient_id);
data = sample_path+considered_pat+".txt";
with open(sample_path+considered_pat+"_purity.txt") as f:
  purities = f.read().split(" ");

import pandas as pd;

df = pd.read_csv(data);
print("df shape: ",df.shape);

df_sample_tags = ["_1","_2"];

df_dict = {
  "chr" : "chr",
  "SNV_pos" : "pos",
  "CancerCellAF" : "ccf",
  "BiopsySequential" : "sample",
  "all_reads" : "reads",
  "ref_reads" : "ref",
  "alt_reads" : "alt",
  "AbsoluteCNA" : "sc_cn",
  "variantPloidy" : "mut_cn",
  "mostLike_nChr" : "multiplicity",
}

def run():
  runPhyloWGS(df,df_dict,df_sample_tags,"",subset_size = 2000);

def runPhyloWGS(df,df_dict,df_sample_tags,output_tag,subset_size = None):
  
  pwgs_format = ["pwgs_pos","pwgs_gen","pwgs_ref","pwgs_rds","pwgs_mur","pwgs_muv"];
  pwgs_header = ["id","gene","a","d","mu_r","mu_v"];
  
  def makeToolFormatSNV(df,dic,tags,toolformat = ["chr","pos","ccf"]):
    #translate the df into standard format
    for tag in tags:
      for key,val in dic.items():
        df[val+tag] = df[key+tag];
        del df[key+tag];
    
    #produce possible other input times
    func = dict();
    
    func["pwgs_pos"] = lambda: "s"+df.index.astype(str);
    func["pwgs_gen"] = lambda: "S"+df.index.astype(str);
    func["pwgs_ref"] = lambda: ((1-df["ccf"+tags[0]]/2)*df["reads"+tags[0]]).astype(int).astype(str) + \
                               "," + \
                               ((1-df["ccf"+tags[1]]/2)*df["reads"+tags[1]]).astype(int).astype(str);
    func["pwgs_rds"] = lambda: df["reads"+tags[0]].astype(str) + "," + df["reads"+tags[1]].astype(str);
    func["pwgs_mur"] = lambda: .999;
    func["pwgs_muv"] = lambda: .499;
    
    #create information that is required by tool  
    for col in toolformat:
      if col not in df.columns:
        df[col] = func[col]();
    
    #put the columns in the right order and select only necessary columns for tool
    df = df[toolformat];
    
    return df;
  
  #get filename
  filename = sample_path + "tmp_pwgs_input.txt";
  
  df_i = makeToolFormatSNV(df,df_dict,df_sample_tags,pwgs_format);
  df_i.columns = pwgs_header;
  
  #sample random portion of the data
  if subset_size != None:
    df_i = df_i.sample(n=subset_size, random_state = 321);
    df_i.index.to_csv(patient_path + "phylo_sample_indeces.txt");
    df_i = df_i.reset_index(drop = True);
    df_i["id"] = "s"+df_i.index.astype(str);
    df_i["gene"] = "S"+df_i.index.astype(str);
  
  #write to file
  df_i.to_csv(filename,sep = "\t",index = False);
  
  # TODO
  def makeToolFormatCNV(df,dic,tags,toolformat = ["chr","pos","ccf"]):
    #translate the df into standard format
    for tag in tags:
      for key,val in dic.items():
        df[val+tag] = df[key+tag];
        del df[key+tag];
    
    #produce possible other input times
    func = dict();
    
    func["pwgs_pos"] = lambda: "c"+df.index.astype(str);
    func["pwgs_gen"] = lambda: "S"+df.index.astype(str);
    func["pwgs_ref"] = lambda: df["ref"+tags[0]].astype(str) + "," + df["ref"+tags[1]].astype(str);
    func["pwgs_rds"] = lambda: df["reads"+tags[0]].astype(str) + "," + df["reads"+tags[1]].astype(str);
    func["pwgs_mur"] = lambda: .999;
    func["pwgs_muv"] = lambda: .499;
    
    #create information that is required by tool  
    for col in toolformat:
      if col not in df.columns:
        df[col] = func[col]();
    
    #put the columns in the right order and select only necessary columns for tool
    df = df[toolformat];
    
    return df;
  '''  
  # =============================================================================
  # execute phylowgs, and save output in output folder
  # =============================================================================
  #make sure output folder doesn't have old data-files (remove them if necessary)
  os.system("rm -Rf " + sample_path + "/phylowgs_output/data/*");
  #make empty file for (lack of) cnv-measurement
  os.system("touch " + sample_path + "tmp_pwgs_empty.txt");
  
  os.chdir(pwgs_path);
  #clear the way, remove the chains folder if there
  os.system("rm -Rf ./chains");
  pwgs_samples = 25;
  pwgs_burnins = pwgs_samples//2;
  #execute phylowgs inside pwgs-folder to get easy settings
  os.system("python2 multievolve.py --num-chains 10 \
            --ssms " + sample_path + "tmp_pwgs_input.txt \
            --cnvs " + sample_path + "tmp_pwgs_empty.txt \
            --chain-inclusion-factor 1.5 \
            --burnin-samples " + str(pwgs_burnins) + " --mcmc-samples " + str(pwgs_samples));
  
  #make visual results
  pwgs_output_name = "niels_run";
  os.system("./niels_make.sh " + pwgs_output_name);
  
  #copy link to visual results to the output folder
  os.system("cp -r ./witness/data/index.json " + rrg_path + "output/phylowgs_output/data");
  os.system("cp -r ./witness/data/" + pwgs_output_name + " " + rrg_path + "output/phylowgs_output/data");
  
  os.system("rm -Rf " + patient_path + "phylowgs_output/"  + considered_pat + output_tag);
  os.system("mkdir " + patient_path + "phylowgs_output/"  + considered_pat + output_tag);
  os.system("cp -r " + rrg_path + "output/phylowgs_output/* " + patient_path + "phylowgs_output/"  + considered_pat + output_tag);
  
  #remove all temporary text files
  os.chdir(sample_path);
  os.system("rm -f  ./tmp_pwgs_input.txt ./tmp_pwgs_empty.txt");'''


def runDPClust(df,df_dict,df_sample_tags,output_tag):
  dpc_format = ["dpc_chr","pos","ref","alt","sc_cn","mut_cn","ccf","multiplicity","dpc_phs"];
  dpc_header = ["chr","end","WT.count","mut.count","subclonal.CN",
                "mutation.copy.number","subclonal.fraction","no.chrs.bearing.mut","phase"];
  
  def makeToolFormat(df,dic,tag,toolformat = ["chr","pos","ccf"]):
    #translate the df into standard format
    for key,val in dic.items():
      df[val] = df[key+tag];
      del df[key+tag];
    
    #produce possible other input times
    func = dict();
    
    func["dpc_chr"] = lambda: df["chr"].str.strip().str[3:];
    func["dpc_1"] = lambda: 1;
    func["dpc_2"] = lambda: 2;
    func["dpc_phs"] = lambda: "unphased";
    
    #create information that is required by tool  
    for col in toolformat:
      if col not in df.columns:
        df[col] = func[col]();
    
    #put the columns in the right order and select only necessary columns for tool
    df = df[toolformat];
    
    return df;
  
  os.chdir(sample_path);
  
  filenames = [];
  for sample_tag in df_sample_tags:
    print("Convert sample", df_sample_tags.index(sample_tag));
    df_i = makeToolFormat(df.copy(),df_dict,sample_tag,toolformat = dpc_format);
    df_i.columns = dpc_header;
    
    filenames.append("real"+sample_tag+".txt");
  
    df_i.to_csv(filenames[-1],sep = "\t",index = False)
  
  #make meta input file containing all the sample input file names
  meta_header = "\t".join(["sample","subsample","datafile","cellularity"])
  
  sample_indicators = ["sampleset" for sampleID in range(len(filenames))];
  sample_numbers = [f"{sampleID:02}" for sampleID in range(len(filenames))];
  #filenames
  cellularity = purities;#["1" for sampleID in range(len(filenames))];
  
  meta_df = list(zip(sample_indicators,sample_numbers,filenames,cellularity));
  
  #write meta file
  meta_filename = "tmp_dpc_meta_input.txt";
    
  with open(meta_filename,'w') as file:
    #write header
    file.write(meta_header+"\n");
    
    #write data rows from meta dataframe
    for row in meta_df[:-1]:
      file.write("\t".join(row));
      file.write("\n");
    file.write("\t".join(meta_df[-1]));
    
  ##RUN DPClust
  
  #make sure output folder is available and empty
  if not os.path.exists(sample_path + "dpclust_output"):
    os.mkdir(sample_path + "dpclust_output")
  os.system("rm -Rf ./dpclust_output/*");
  
  #execute dpclust_pipeline locally
  dpc_samples = 250;
  dpc_burnins = dpc_samples//2;
  os.chdir(rrg_path + "programpipes/")#dpc_path + "example/");
  run_string = "R --vanilla --slave -q -f dpclust_pipeline.R --args -r 1 --iterations " +str(dpc_samples)+" --burnin "+str(dpc_burnins)+" -d " + sample_path[:-1] + " -o " + sample_path + "dpclust_output -i " + sample_path + meta_filename;
  os.system(run_string);
  
  #remove all temporary text files
  os.system("rm -f " + sample_path + "/real*.txt");
  os.system("rm -f " + sample_path + considered_pat + "*.txt");
  os.system("rm -f " + sample_path + "/tmp_dpc*.txt");
  
  # #mv the data to the patient_data folder
  os.system("mv " + sample_path + "dpclust_output/*/*.pdf " +
            patient_path + "dpclust_output/dpc_plot_" + considered_pat + output_tag + ".pdf");
  os.system("mv " + sample_path + "dpclust_output/*/*assignments.png " +
            patient_path + "dpclust_output/dpc_data_" + considered_pat + output_tag + ".png");
  os.system("mv " + sample_path + "dpclust_output/* " +
            patient_path + "dpclust_output/dpc_more_" + considered_pat + output_tag);

run();
