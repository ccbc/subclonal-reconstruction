import sys;
import os;
import getPaths;

#get all paths
rrg_path = getPaths.paths["rrg"];
tc_path = getPaths.paths["targetclone"];
sample_path = rrg_path + "output/";

#get file names of data files
os.chdir(sample_path);
data_file_prefix = "reads_data"
sample_files = [f for f in os.listdir(".") if f[:len(data_file_prefix)] == data_file_prefix];
sample_tags = ["final_sample" if f[-5:]=="a.txt" else "sample_"+f[-5] for f in sample_files];

#TODO: sort files
# #sort them
# sample_tags = [0 if f[-5:]=="a.txt" else int(f[-5]) for f in sample_files];
# sample_files = [x for _,x in sorted(zip(sample_tags,sample_files))];
# sample_files.append(sample_files.pop(0));

#extract vafs from files using "ReadsToVAF.py"
os.chdir(rrg_path);
all_vafs = [];
for sample_fileID in range(len(sample_files)):
  os.system("cat output/" + sample_files[sample_fileID] + " | python3 ReadsToCCFs.py > tmp_tc.txt");
  with open("tmp_tc.txt","r") as tmp_file:
    line = tmp_file.readlines()[0];
    vafs = [];
    for vaf in line.split(","):
      vafs.append(vaf);
    #remove "\n"
    vafs[-1] = vafs[-1][:-2];
  all_vafs.append(vafs);
  
  #delete tmp_tc.txt file
  os.system("rm -f ./tmp_tc.txt");

def SetTCValues(vaf,locus,chrm,pos,ref,smp):
  #CHR
  chrm.append('1');
  
  #POS
  pos.append(str(locus));
    
  #SMP
  smp.append(str(round(.5+float(vaf)/2,2)));
  
  #REF
  ref.append('0.5');

all_chrm = [];
all_pos = [];
all_ref = [];
all_smp = [];

#Extract TC-relavent info from calls:
for sampleID in range(len(sample_files)):
  chrm = [];
  pos = [];
  ref = [];
  smp = [];

  for vafID in range(len(all_vafs[sampleID])):
    SetTCValues(all_vafs[sampleID][vafID],vafID,chrm,pos,ref,smp);
  
  all_chrm.append(chrm);
  all_pos.append(pos);
  all_ref.append(ref);
  all_smp.append(smp);

#somatic variant list (all "N" (no) since these are VAFs)
all_snvindications = ["N"]*len(all_chrm[0]);

#make df
df = list(zip(all_chrm[0],all_pos[0],all_snvindications,all_ref[0],*all_smp));

#make header for tc file
header = "\t".join(["Chromosome","Start","Somatic variant","Reference"]+sample_tags);
# print(header)

#add fake SNV measurement (not needed if you turn off the SNV setting in the tc "settings.py")
#however does relieve division by zero error
snv_row = "\t".join(["2","1","Y","0"]+["1"]*len(sample_files));

#get filename
filename = rrg_path + "tmp_tc_input.txt";

#write to file
with open(filename,'w') as file:
  #write header
  file.write(header+"\n");

  #write data file rows
  for df_row in df[:-1]:
      # print("df_row:\n",df_row);
      file.write('\t'.join(df_row));
      file.write('\n');
  file.write('\t'.join(df[-1]));
  file.write("\n"+snv_row);
  
# =============================================================================
# execute targetclone to output folder
# =============================================================================

#make sure output folder doesn't have tc-files (remove them if necessary)
os.system("rm -f ./output/targetclone_output/*");

#execute targetclone inside tc-folder to get easy settings
os.chdir(tc_path);
os.system("python2 main.py " + rrg_path + "tmp_tc_input.txt " + rrg_path + "output/targetclone_output/");

#remove all temporary text files
os.chdir(rrg_path);
os.system("rm -f ./tmp_tc_input.txt");
