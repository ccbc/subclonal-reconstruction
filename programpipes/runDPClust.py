import sys;
import os;
import getPaths;

if len(sys.argv) > 2:
  mcmc_samples = int(sys.argv[2]);
else:
  mcmc_samples = 25;

#get all paths
rrg_path = getPaths.paths["rrg"];
dpc_path = getPaths.paths["dpclust"];
sample_path = rrg_path + "output/";

#if process_id is indicated, run multiple of the dpclust_pipelines in parallel
if len(sys.argv) > 1:
  process_id = int(sys.argv[1]);
  sample_path = rrg_path + "output/process_" + str(process_id) + "/";
else:
  process_id = None;

#get file names of data files
os.chdir(sample_path);
data_file_prefix = "reads_data"
sample_files = [f for f in os.listdir(".") if f[:len(data_file_prefix)] == data_file_prefix];
sample_tags = [0 if f[-5:]=="a.txt" else int(f[-5]) for f in sample_files];
with open("reads_data.txt") as f:
  sample_readN = list(enumerate(f))[-1][0];
  
#sort them
sample_files = [x for _,x in sorted(zip(sample_tags,sample_files))];
sample_files.append(sample_files.pop(0));

#extract vafs from files using "ReadsToVAF.py"
os.chdir(sample_path);
all_vafs = [];
for sample_fileID in range(len(sample_files)):
  os.system("cat " + sample_files[sample_fileID] + " | python3 " + rrg_path + "ReadsToCCFs.py nozero > tmp_dpc.txt");
  with open("tmp_dpc.txt","r") as tmp_file:
    line = tmp_file.readlines()[0];
    vafs = [];
    for vaf in line.split(","):
      vafs.append(vaf);
    #remove "\n"
    vafs[-1] = vafs[-1][:-2];
  all_vafs.append(vafs);
  
  #delete tmp_dpc.txt files
  os.system("rm -f ./tmp_dpc.txt");
  
def SetDPCValues(readN,vafID,vaf,wt,mut,sc_frac):
  #get reads that are mutated
  mut_reads = int(readN * float(vaf)/2);
  
  #wt
  wt.append( readN - mut_reads );
  
  #mut
  mut.append(mut_reads);
  
  #sc_frac
  sc_frac.append(vaf);

#lists for all samples
all_wt = [];
all_mut = [];
all_sc_frac = [];

#Extract DPClust-relavent info from calls:
for sampleID in range(len(sample_files)):
  wt = [];
  mut = [];
  sc_frac = [];
  

  for vafID in range(len(all_vafs[sampleID])):
    SetDPCValues(sample_readN,
                  vafID,
                  all_vafs[sampleID][vafID],
                  wt,
                  mut,
                  sc_frac)
  
  all_wt.append(wt);
  all_mut.append(mut);
  all_sc_frac.append(sc_frac);

def MakeListWith(s,c=0):
  return([[str(s+i*c) for i in range(len(all_vafs[sampleID]))] for sampleID in range(len(sample_files))]);

all_chrm = MakeListWith(1);
all_end = MakeListWith(0,1);
#wt
#mut
all_sc_cn = MakeListWith(2);
all_mut_cn = all_sc_frac;
#sc_frac
all_chrm_mut = MakeListWith(1);
all_phase = [["unphased" for i in range(len(all_vafs[sampleID]))] for sampleID in range(len(sample_files))]

#remove the VAF that are to low to speed up analysis
CUTOFF = 0.05 #5% cutoff (lower than 5% of reads is mutated -> VAF not considered), this speeds up the analysis considerably (less datapoints)
vaf_loci = list(zip( *all_wt ));
loci_sums = list(map(sum,vaf_loci));

#remove all calls with wildtype (wt) count above "1-cutoff" (less than 5% variant)
excluded_loci = [];
for locus_ID in reversed(range(len(loci_sums))):
  if loci_sums[locus_ID] >= (sample_readN*(1-CUTOFF))*len(sample_files):
    excluded_loci.append(locus_ID);

#make df
df = []; #dataframe per sample
for sampleID in range(len(sample_files)):
  sample_df = list(zip(all_chrm[0],all_end[0],list(map(str,all_wt[sampleID])),list(map(str,all_mut[sampleID])),
                all_sc_cn[0],all_mut_cn[0],all_sc_frac[0],all_chrm_mut[0],all_phase[0]));
  
  for locus_ID in excluded_loci:
    del sample_df[locus_ID];
  
  df.append(sample_df); 

#make header for tc file
header = "\t".join(["chr","end","WT.count","mut.count","subclonal.CN","mutation.copy.number","subclonal.fraction","no.chrs.bearing.mut","phase"]);
# print(header)

#make input file for each sample
filenames = [];
for sampleID in range(len(sample_files)):
  #get filename
  filename = "tmp_dpc_sample_" + str(sampleID) + ".txt";
  filenames.append(filename);
  
  #write to file
  with open(sample_path + filenames[-1],'w') as file:
    #write header
    file.write(header+"\n");
  
    #write data file rows
    for df_row in df[sampleID][:-1]:
        # print("df_row:\n",df_row);
        file.write('\t'.join(df_row));
        file.write('\n');
    file.write('\t'.join(df[sampleID][-1]));

#make meta input file containing all the sample input file names
meta_header = "\t".join(["sample","subsample","datafile","cellularity"])

sample_indicators = ["sampleset" for sampleID in range(len(sample_files))];
sample_numbers = [f"{sampleID:02}" for sampleID in range(len(sample_files))];
#filenames
cellularity = ["1" for sampleID in range(len(sample_files))];

meta_df = list(zip(sample_indicators,sample_numbers,filenames,cellularity));

#write meta file
meta_filename = "tmp_dpc_meta_input.txt";
  
with open(meta_filename,'w') as file:
  #write header
  file.write(meta_header+"\n");
  
  #write data rows from meta dataframe
  for row in meta_df[:-1]:
    file.write("\t".join(row));
    file.write("\n");
  file.write("\t".join(meta_df[-1]));

# =============================================================================
# execute DPClust, and save output in output folder
# =============================================================================

#make sure output folder is available and empty
if not os.path.exists(sample_path + "dpclust_output"):
  os.mkdir(sample_path + "dpclust_output")
os.system("rm -Rf ./dpclust_output/*");

#execute dpclust_pipeline locally
dpc_samples = mcmc_samples;
dpc_burnins = mcmc_samples//2;
os.chdir(rrg_path + "programpipes/")#dpc_path + "example/");
run_string = "R --vanilla --slave -q -f dpclust_pipeline.R --args -r 1 --iterations " +str(dpc_samples)+" --burnin "+str(dpc_burnins)+" -d " + sample_path[:-1] + " -o " + sample_path + "dpclust_output -i " + sample_path + meta_filename;
os.system(run_string);

#remove all temporary text files
os.chdir(sample_path);

os.system("rm -f  ./tmp_dpc*.txt");
