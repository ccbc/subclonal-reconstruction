# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 20:31:48 2020

@author: niels
"""
DEBUG = 0;
import numpy as np;
import numpy.random as rnd;
from settings import settings;
from settings import general;
import sys;

def Out(s):
    old_stdout = sys.stdout;
    sys.stdout = sys.__stdout__
    print(s);
    sys.stdout = old_stdout;

def ChangePloidy(readset,chances):
    #double
    if rnd.rand() < chances[0] and len(readset) == 2:
        set_size = dim(readset)[0];
        for i in range(set_size):
            readset.append(readset[i]);
    #halve
    if rnd.rand() < chances[1] and len(readset) == 2:
        for i in range(int(len(readset)/2)):
            readset.pop();
    #plus one
    if rnd.rand() < chances[2]:
        read_add = rnd.choice(readset);
        readset.append(read_add);
    #minus one
    if rnd.rand() < chances[3]:
        read_add = rnd.choice(readset);
        readset.append(read_add);

def AdaptPloidy(new_readset,old_readset):
    for i in range(len(old_readset)-len(new_readset)):
        new_readset.append([]);

def DuplicateHaploid(
        reads,
        t,
        cellN,
        cell_muts,
        cell_parents,
        mutRate,
        mut_mat,
        mut_mask_mat,
        drivers,
        driverMuts,
        bigeventID,
        bigevent_settings,
        bigexcludes,
        selectionMask,
        seed_gen
        ):
    
    det_bigevent = bigevent_settings[0];
    bigevent_with_driver = bigevent_settings[1];
    if bigevent_settings[0] == True: #deterministic bigevents
        bigevent_parents,bigevent_times, bigevent_percentage,bigevent_percentage_stdv = bigevent_settings[2:];
    else: #non-deterministic bigevents
        bigevent_parents,bigevent_chance,bigevent_percentage,bigevent_percentage_stdv = bigevent_settings[2:]; #chances=(big event chance, big event population percentage)
    
    bigevent = -1; #>-1 if there is a big event, represents cellID that has big event
    #drivers variable #records if drivermutation occured TODO: add case if driver mut not in big event
    #pick cell that gets to clonaly expand = big event
    if t in bigevent_times:
        #force phylogenetic relation if indicated
        if len(bigevent_parents) > 0:
            for cellID in range(cellN):
                if reads[cellID][0] == bigevent_parents[bigeventID]:
                    bigevent = cellID;
            if bigevent == -1:
                Out("ERROR: Not enough cells, enforcing phylogeny is too hard");
                exit();
        #if no phylogeny indicated
        else:
            bigevent = rnd.randint(cellN);
        
        #add to bigeventID
        bigeventID += 1;
    
    daughterN = 2;
    daughter_reads = [];
    daughter_parents = [];
    daughter_chances = [];
    
    nuclN = len(reads);
    
    #record new mutations in list
    next_cell_muts = [None]*nuclN;
    #record parents in list
    next_cell_parents = [None]*cellN;
    
    #make mutation mask (mut_mask_mat) and record mutations (muts_matrix) for all cells
    if mut_mat == None and mut_mask_mat == None:
        mut_mat,mut_mask_mat = MutBool(mutRate,cell_number = cellN);
    
    #for all cells make two daughter cells
    for cell in range(cellN):
        if DEBUG:
            Dbg("Cell " + str(cell));
            Dbg(Binary(reads[cell]));
            
            Dbg("parent's acquired mutations:");
            Dbg(cell_muts[cell]);
            
            Dbg("Daughters:");
        
        #possible next_reads are generated, here also record muts, which might later be appended to cell_muts if one of daughters survive
        mut,mut_mask = list(mut_mat[cell]),mut_mask_mat[cell]; #MutBool(mutRate);
        if len(mut) > 0:
            # print("mut: ",mut,"\t\t\t\t <-- Mutation(s)")
            next_cell_muts[cell] = mut;
            print("Mutations: ", mut);
        
        #add big duplication event, here big procentage of population is replaced with one cell's daughter cells
        if ((det_bigevent == False and rnd.sample() < bigevent_chance and bigevent == -1)
            or (det_bigevent == True and bigevent == cell)):
            
            new_pop_frac = bigevent_percentage+bigevent_percentage_stdv*rnd.randn();
            
            print("BIG EVENT HAPPENING IN CELL " + str(cell) + " AT TIME " + str(t));
            daughterN = int(max(0,min(1,new_pop_frac))*cellN);
            bigevent = cell;
            
            #retag initial nucleotide (new population)
            reads[cell][0] = bigeventID;
            print("NEW POPULATION (" + str(new_pop_frac) + "%) of total) : ",bigeventID);
            
            #add driver mutation to mutation mask if specified
            if bigevent_with_driver:
                #make sure it is not already aquired somewhere
                potential_driverMutIDs = [];
                for driverID in range(len(drivers)):
                    if drivers[driverID] == None:
                        potential_driverMutIDs.append(driverID);
                
                driverMutID = rnd.choice(potential_driverMutIDs);
                driverMut = driverMuts[driverMutID];
                
                #add it to mask
                print("ADD DRIVER (big event): ", driverMut);
                mut_mask[driverMut] = True;
                mut.append(driverMut);
                drivers[driverMutID] = t;
            
            #add passenger snvs to big event if specified
            bigsnv_fraction = settings.rrg["bigevent_snv_fraction"]
            if bigsnv_fraction > 0:
                bigmutRate = np.ones(len(mutRate));
                
                #make sure it does not aqcuire a previous subclone mutation
                for m in bigexcludes:
                    bigmutRate[m] = 0;
                
                #correct for excluded mutation sites (normalize) and set the mutation fraction
                bigmutRate = bigmutRate * len(bigmutRate)/sum(bigmutRate) *bigsnv_fraction;
                
                bigmut,bigmut_mask = MutBool(bigmutRate);
                print("ADD SNVs (big event): ", bigmut);
                mut_mask = mut_mask | bigmut_mask;
                mut += bigmut;
                
                #No ISA violation, no parallel aquirement of mutations
                for m in bigmut:
                    bigexcludes.append(m);
        
        #copy info from parent, add new mutations (first is tag nucleotide)
        next_reads = [reads[cell][0]] + list(mut_mask | np.array(reads[cell][1:], dtype=bool));

        
        for daughter in range(daughterN):
            daughter_reads.append(next_reads);
            
            #add survival chance based on mask containing driver mutations
            survival_chance = 1 if bigevent == cell else np.dot(next_reads[1:],selectionMask);
            daughter_chances.append(survival_chance);
            
            #add parent of daughter for phylogenetic reconstruction
            daughter_parents.append(cell);
        
        #reset the amount of daughters to two
        daughterN = 2;
            
        if DEBUG:
            Dbg(Binary(next_reads));
            Dbg("Additional survival chance: " + str(survival_chance));
            Dbg("---");
    
    #select from daughter cell the cell that survive
    if DEBUG:
        Dbg("Selection of daughter reads:");
    next_cells, next_cell_parents = Selection(daughter_reads,daughter_parents,daughter_chances,nuclN);
    
    return next_cell_muts,next_cell_parents,next_cells,drivers,bigevent,bigeventID, bigexcludes;

def Selection(cells,parents,chances,size):
    new_cells = []; #reads of cells that have survived
    new_cell_parents = []; #parents of survived cells
    survived = []; #ids of cells that have survived
    cellID = 0;
    loop = 1; #iteration of search for new cells
    
    #make sure that cells with "1" survival chance are first
    first_indices = [];
    remaining_indices = [];
    for chanceID in range(len(chances)):
        if chances[chanceID] == 1:
            first_indices.append(chanceID);
        else:
            remaining_indices.append(chanceID);
            
    #shuffle remaining cells, for fair selection
    rnd.shuffle(remaining_indices);
    all_indices = first_indices + remaining_indices;
    
    cells = [cells[i] for i in all_indices];
    parents = [parents[i] for i in all_indices];
    chances = [chances[i] for i in all_indices];
    
    #do untill enough new cells
    while len(new_cells) < size:
        if cellID not in survived:
            cell = cells[cellID];
            
            survival_chance = chances[cellID]*(1-loop*.01)+loop*.01;
            if rnd.sample() < survival_chance:
                Dbg("Cell " + str(cellID) + " survived with chance: " + str(survival_chance) + " in loop " + str(loop));
                new_cells.append(list(cell));
                new_cell_parents.append(parents[cellID]);
                survived.append(cellID);
        
        if cellID >= len(cells)-1:
            cellID = 0;
            loop+=1;
        else:
            cellID += 1;
    
    if DEBUG:
        Dbg("All cells that have survived: ");
        for new_cell in new_cells:
            Dbg(Binary(new_cell));
        
    return new_cells, new_cell_parents;
    
def GetPrevalence(reads,bigeventN):
    populations = [b for b in range(bigeventN+1)]; #the initial "population/subclone" tags of the reads
    prevalences = [0 for b in range(bigeventN+1)]; #the corresponding prevalence of the population in the reads, its weight
    
    for read in reads:
        prevalences[read[0]] += 1;
    
    readN = len(reads);
    return [prevalence/readN for prevalence in prevalences];

def MutBool(pmut, cell_number = None, time_number = None, seed_gen = None):
    if time_number != None:
        random_numbers = rnd.ranf((time_number,cell_number,pmut.size));
        
        read = np.greater(pmut,random_numbers);
        muts = [[np.argwhere(read[t][cell] == True) for cell in range(cell_number)] for t in range(time_number)]
    else:
        if cell_number != None:
            random_numbers = rnd.ranf((cell_number,pmut.size));
            
            read = np.greater(pmut,random_numbers);
            muts = [np.argwhere(read[cellID] == True) for cellID in range(cell_number)]
            
        else:
            muts = [];
        
            if seed_gen != None:
                string_size = 20;
                string_gen = next(seed_gen);
                random_numbers = [float(string_gen[i : i+string_size]) for i in range(0,len(string_gen),string_size)];
            else:
                random_numbers = rnd.ranf(pmut.size);
            
            read = np.greater(pmut,random_numbers);
        
            #possible mutate all be the first (=tag) nucleotide
            for i in range(pmut.size):
                if read[i] == True:
                    Dbg("Mutation in nucleotide " + str(i));
                    muts.append(i);
            
        
    return(muts,read);

def dim(a):
    if not type(a) == list:
        return []
    return [len(a)] + dim(a[0])

def Binary(a):
    return list(map(int,a));

def Dbg(s):
    if DEBUG:
        print(s);
