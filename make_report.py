from fpdf import FPDF
import os;

title = 'Statistical analysis report'

class PDF(FPDF):
    start_x = 5;
    start_y = 25;
    start_w = 200;
    start_h = 276;
    
    pipe_ccfs_figs = None;
    ccfs_hist_figs = None;
    
    def header(self):
        
        xx, yy ,ww, hh = self.get_dims();
        self.set_y(yy//2);
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Calculate width of title and position
        w = self.get_string_width(title) + 6
        self.set_x((210 - w) / 2)
        # Colors of frame, background and text
        self.set_draw_color(100, 255, 100)
        self.set_fill_color(255, 255, 255)
        self.set_text_color(100, 255, 100)
        # Thickness of frame (1 mm)
        self.set_line_width(1)
        # Title
        self.cell(w, 9, title, 1, 1, 'C', 1)
        # Line break
        self.ln(10)
    
    def page_title(self,title):
        # Arial bold 15
        self.set_font('Arial', 'B', 15)
        # Calculate width of title and position
        w = self.get_string_width(title) + 6
        self.set_x((210 - w) / 2)
        # Colors of frame, background and text
        self.set_draw_color(0, 102, 204)
        self.set_fill_color(0, 255, 255)
        self.set_text_color(0, 0, 0)
        # Thickness of frame (1 mm)
        self.set_line_width(1)
        # Title
        self.cell(w, 9, title, 1, 1, 'C', 1)
        # Line break
        self.ln(10)

    def footer(self):
        # Position at 1.5 cm from bottom
        self.set_y(-15)
        # Arial italic 8
        self.set_font('Arial', 'I', 8)
        # Text color in gray
        self.set_text_color(128)
        # Page number
        self.cell(0, 10, 'Page ' + str(self.page_no()), 0, 0, 'C')
    
    def get_dims(self):
        return(self.start_x,self.start_y,self.start_w,self.start_h);
        
    def draw_point(self,xx,yy):
        self.set_xy(xx,yy);
        self.set_fill_color(255, 0, 0)
        self.cell(5, 5, fill = 1)
    
    def simulation_page(self,title,fishplot,vafplots,pipes,correct_sc_n,error,subloops):
        self.add_page();
        xx, yy ,ww, hh = self.get_dims();
        
        # title
        self.set_xy(xx,yy);
        self.set_font_size(10)
        self.set_fill_color(200, 220, 255)
        self.cell(0, 6, 'Simulation - %s' % (title), 0, 1, 'L', 1)
        yy += 6;
        
        #main simulation figure
        if os.path.isfile(fishplot):
          self.image(fishplot,x=3,y=yy,w=200,h=100);
        yy += 102;
        
        #vafs plots
        for i in range(len(vafplots)):
          xi = xx+i*ww//len(vafplots);
          
          self.set_xy(xi,yy);
          self.set_font_size(10);
          self.set_fill_color(50, 50, 255);
          self.cell(ww//len(vafplots)-2, 6, 'Sample - %s' % (str(i)));
          yy += 6;
          if os.path.isfile(vafplots[i]):
            self.image(vafplots[i],x=xi,y=yy,w=ww//len(vafplots)-2,h=30);
          yy -= 6;
        yy += 40;
        
        #pipes table
        header = ["subloops:"+str(subloops), "correct subclones-# estimates","ccf error"];
        rows = list( zip(pipes, correct_sc_n, error) );
        df = [header] + rows;
        
        wi = ww//len(header);
        hi = (hh-yy)//len(df);
        for j in range(len(df)):
          for i in range(len(header)):
            xi = xx+wi*i;
            
            self.set_xy(xi,yy+hi*j);
            self.cell(wi,hi,str(df[j][i]),border=1, align='C');

    def parameter_page(self,title,par_plot,pipes,parameter_space,correct_sc_n,error,loops,subloops):
        self.add_page();
        xx, yy ,ww, hh = self.get_dims();
        
        # title
        self.set_xy(xx,yy);
        self.set_font_size(10)
        self.set_fill_color(200, 220, 255)
        self.cell(0, 6, 'Parameter estimates - %s' % (title), 0, 1, 'L', 1)
        yy += 6;
        
        #main simulation figure
        self.image(par_plot,x=3,y=yy,w=200,h=100);
        yy += 102;
        
        #pipes table
        par_headers = sum([["c-" + str(par_id)] for par_id in parameter_space], []); #TODO: , "e-" + str(par_id)
        header = ["subloops\nx\nloops:\n"+str(subloops*loops)] + par_headers;
        rows = list( zip(pipes, *correct_sc_n) ); #TODO: *error
        df = [header] + rows;
        
        wi = ww//len(header);
        hi = (hh-yy)//len(df);
        for j in range(len(df)):
          for i in range(len(header)):
            xi = xx+wi*i;
            
            self.set_xy(xi,yy+hi*j);
            self.cell(wi,hi,str(df[j][i]),border=1,align='C');

    def dump_text_page(self,text):
        self.add_page();
        xx, yy ,ww, hh = self.get_dims();
        
        self.set_xy(xx,yy);
        self.multi_cell(ww,6,str(text),border=1,align='L');

    def figure_title(self, par, tag, title, x = 0, y = 0):
        self.set_xy(x,y);
        # Arial 12
        self.set_font('Arial', '', 12)
        # Background color
        self.set_fill_color(200, 220, 255)
        # Title
        self.cell(0, 6, '%s : %s, tag=%s' % (title, par, tag), 0, 1, 'L', 1)

    def print_simulation(self, par, tag, name):
        self.add_page();
        self.pipe_ccfs_figs = 0;
        self.ccfs_hist_figs = 0;
        self.figure_title(par, tag, "simulation",self.start_x,self.start_y);
        self.image(name,x=self.start_x,y=self.start_y+10,w=200,h=100,type="",link="");
    
    def print_ccfs_hist(self, name, title):
        hh = 60;
        
        xx = self.start_x + self.ccfs_hist_figs%2 * 100;
        yy = self.start_y + 110 + self.ccfs_hist_figs//2 * hh;
        
        self.set_xy(xx,yy);
        # Arial 12
        self.set_font('Arial', '', 12);
        # Background color
        self.set_fill_color(200, 220, 255);
        # Title
        self.cell(0, 6, title);
        
        self.image(name,x=xx,y=yy+10,\
                   w=100,h=hh-10,type="",link="");
        
        self.ccfs_hist_figs += 1;
        
        if self.ccfs_hist_figs == 4:
          self.add_page();
          self.ccfs_hist_figs = -2;
      
    def print_pipe_ccfs(self, par, tag, name, title):
        hh = 50;
        
        xx = self.start_x + self.pipe_ccfs_figs%2 * 100;
        yy = self.start_y + 110 + self.pipe_ccfs_figs//2 * hh;
        
        self.figure_title(par, tag, "analysis of " + title,xx,yy);
        self.image(name,x=xx,y=yy+10,\
                   w=100,h=hh-10,type="",link="");
        
        self.pipe_ccfs_figs += 1;
        
        if self.pipe_ccfs_figs == 6:
          self.add_page();
          self.pipe_ccfs_figs = -4;
        
    def print_stats_plot(self, name):
        self.image(name,x=self.start_x,y=self.start_y,w=200,h=100,type="",link="");
        
    def print_stats_table(self, title, stats, xlabel, ylabel, xx , yy):
        ww = 20;
        hh = 5;
        
        self.set_xy(xx,yy);
        yy += 15; 
        # Arial 12
        self.set_font('Arial', '', 12)
        # Background color
        self.set_fill_color(200, 220, 255)
        # Title
        self.cell(0, 6, '%s' % (title), 0, 1, 'L', 1)
        
        labeled_stats = list(zip(ylabel,*stats))
        df = [ ["Loop\\Par."] + xlabel] + labeled_stats;
                
        for j in range(len(ylabel)+1):
          for i in range(len(xlabel)+1):
            self.set_xy(xx+ww*i,yy+hh*j);
            self.cell(ww,hh,str(df[j][i]),border=1);
    
    def init_par(self):
        self.add_page();
    
    def init_loop(self):
        self.add_page();
        self.pipe_ccfs_figs = 0;
    
    def print_data_table(self, title):
        pass;#TODO: print settings and specific pipeline settings
