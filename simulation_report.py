# simulation report
import make_report;
import os;
import sys;

rrg_path = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/randomreadgenerator/";
sample_path = rrg_path + "output/";

make_report.title = "Simulation Report";
pdf = make_report.PDF();

pdf.print_simulation("","",sample_path + "fishPlot.png");
for i in range(int(sys.argv[1])):
  pdf.print_ccfs_hist(sample_path + "densityVAF_" + str(i) + ".png","CCFs at " + str(i+1) + "'th sample");
pdf.print_ccfs_hist(sample_path + "densityVAF.png","CCFs at final sample");

pdf.output(sample_path + "simulation_report.pdf",'F');
