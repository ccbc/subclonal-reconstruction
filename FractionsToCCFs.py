'''Transform the recorded 'truth' fractions to CCFs in a new format'''
#transform prevalences into CCFs (mutation fractions)
import sys;
from math import floor;

PRECISION = .001;

ds_raw = sys.stdin.read();

data_lines = ds_raw.split("\n")[:-1];

#function that floors to 3 decimals precision
def floorn(d):
  m = int(1/PRECISION);
  return floor(d*m)/m;

#first line has the measurement times
times = [];
for time in data_lines[0].split(","):
  times.append(int(time));

#second line has population dependencies
deps = [];
for dep in data_lines[1].split(","):
  deps.append(int(dep));

pops = list(range(len(deps)+1));
pops.reverse();

#rest of the lines are population fractions
all_fracs = [];
for line in data_lines[2:]:
  fracs = [];
  for frac in line.split(","):
    fracs.append(floorn(float(frac)));
  all_fracs.append(fracs);

#transform fractions into CCFs for every timepoint
all_ccfs = [];
for t in range(len(all_fracs)):
  ccfs = all_fracs[t];
  # print(ccfs);
  for pop in pops:
    # print("pop: ",pop);
    for dep in range(len(deps)):
      # print("dep: ",dep)
      if deps[dep] == pop:
        # print("hit, add:", all_fracs[t][dep+1])
        #add ccfs to parent (add residue)
        ccfs[pop] += max(all_fracs[t][dep+1],ccfs[dep+1])+PRECISION;
  # print(ccfs);
  
  #remove residue of parent ccfs (to not exceed the parent ccf)
  subtraction = ccfs[0]-1;
  
  for ccfID in range(len(ccfs)):
    ccfs[ccfID] = max(ccfs[ccfID]-subtraction,0);
  
  all_ccfs.append(ccfs);

#make sure that the ccfs also do not exceed 1 (when added)
for t in range(len(all_ccfs)):
  for ccfID in range(len(all_ccfs[t])):
    all_ccfs[t][ccfID] = floorn(all_ccfs[t][ccfID]);

# print(all_ccfs);

#print all the deps and CCFs (including initial fraction 1,0,0,...)
for time in times[:-1]:
  print(time,end=",");
print(times[-1]);

for dep in deps[:-1]:
  print(dep,end=",");
print(deps[-1]);

print("1",end=",")
for pop in pops[1:-1]:
  print("0",end=",");
print("0")

for ccfs in all_ccfs:
  for ccf in ccfs[:-1]:
    print(ccf,end=",");
  print(ccfs[-1]);

