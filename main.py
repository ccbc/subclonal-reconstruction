# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 20:18:02 2020

@author: niels
"""
import numpy as np;
import numpy.random as rnd;
import time;
from phyloprint import ExportReads, StartLog, ContinueLog, EndLog, PhyloPrint, PrintReads, PrintTimeBlock, Binary, dim, BinaryVisual;
from readchanges import DuplicateHaploid, GetPrevalence, MutBool;
import sys;
from settings import general;

def main(settings,process_id = None,myseed = None,seedfile = None):
    
    #get seedfile if indicated
    if seedfile != None:
      size = settings.rrg["nuclN"];
      string_size = 20;
      
      seedfile_path = "/mnt/data/ccbc_environment/users/nburghoorn/rrg/seedfiles/";
      seedfile_name = "seedfile_0.txt";
      
      def read_chunk(file, chunk_size = 1024):
        while True:
          data = file.read(chunk_size);
          if not data:
            break;
          yield data;
      
      seedfile = open(seedfile_path + seedfile_name,'r');
      seed_gen = read_chunk(seedfile,string_size*size);
    else:
      seed_gen = None;
    
    #get seed
    if myseed == None:
        myseed = rnd.randint(0,1000);
    
    rnd.seed(myseed);
    
    DEBUG = general["DEBUG"];
    PIPE = general["PIPE"];
    FISH = general["FISH"];
    PATH = general["PATH"];
    if process_id != None:
        PATH += "process_" + str(process_id) + "/";
    LOGNAME = general["LOGNAME"];
    LOADBAR = general["LOADBAR"];
    
    def Dbg(s):
        if DEBUG:
            print(s);
            
    def Out(s):
        sys.stdout = sys.__stdout__
        print(s);
        
    def Loadbar(t,timeT):
        sys.stdout = sys.__stdout__
        barchars = 40;
        
        barlen = int(barchars*t/timeT);
        bar = "="*barlen + " "*(barchars-barlen);
        print("simulation progress: [" + bar + "]",end="\r");
        if t == timeT:
            print("");
    
    StartLog(LOGNAME);
    
    print("program started");
    
    start = time.time();
    
    # =============================================================================
    # Set all initial parameters
    # =============================================================================
    
    cellN = settings.rrg["cellN"];
    
    Dbg("Initial cell number = "+str(cellN));
    
    nuclN = settings.rrg["nuclN"]; #number of nucleotides per chromosome to monitor + tag nucleotide (in front)
    timeT = settings.rrg["timeT"]; #number of cell divisions
    
    print("\nestimated data generation time: " + str(1.513898731+4.82492E-05*(2**timeT)*nuclN))
    print("\tgenerating data...\n");
    
    #select driver mutations (mask with chances of survival)
    Dbg("Driver mutations:")
    driverMutN = settings.rrg["driverMutN"];
    driverMuts = [];
    driverMutsFound = 0;
    while driverMutsFound < driverMutN:
        driverMut = rnd.randint(nuclN);
        if driverMut not in driverMuts:
            driverMuts.append(driverMut);
            driverMutsFound+=1;
    
    #make selection mask with driver mutations
    snv_penalty = settings.rrg["snv_selection_penalty"];
    driver_reward = settings.rrg["driver_selection_reward"]/driverMutN if driverMutN > 0 else 0;
    
    selectionMask = [snv_penalty for i in range(nuclN)];
    for driverMut in driverMuts:
        selectionMask[driverMut] = driver_reward;
    Dbg(driverMuts);
    Dbg("Selection mask:");
    Dbg(selectionMask);
    
    #mutation parameters
    Dbg("Mutate for " + str(timeT) + " cell divisions");
    
    mutRate_low_bound = settings.rrg["snv_mutation_fraction"]/(nuclN*timeT);
    mutRate_high_bound = settings.rrg["snv_mutation_fraction"]/(nuclN*timeT);
    
    #make mutation ratios for each nucleotide (not first nucleotide = tag nucleotide)
    mutRate = rnd.sample(nuclN)*(mutRate_high_bound-mutRate_low_bound)+mutRate_low_bound;
    
    #exlude driver mutations from the normal mutation process if specified
    if not settings.rrg["snv_canbe_driver"]:
        for mutID in range(len(mutRate)):
            if mutID in driverMuts:
                mutRate[mutID] = 0;
    
    #show mutation rates        
    np.set_printoptions(precision=3);
    Dbg("With mutation rates (rounded shown):\n" + str(mutRate));
    np.set_printoptions(precision=8);
    
    #ploidy change chances
    Cn_double_chance = 1e-1;
    Cn_halve_chance = .0; #doesn't work yet
    Cn_p_one_chance = .0; #doesn't work yet
    Cn_m_one_chance = .0; #doesn't work yet
    Cn_chances = np.array([Cn_double_chance,Cn_halve_chance,Cn_p_one_chance,Cn_m_one_chance]);
    Dbg("With ploidy change chances:\n" + str(Cn_chances));
    
    #big event, where a single cell is now representing a large percentage of the population
    deterministic_bigevent = settings.rrg["deterministic_bigevent"];
    bigevent_with_driver = settings.rrg["bigevent_with_driver"];
    bigevent_parents = settings.rrg["bigevent_parents"]; #phylogenetic relation between populations
    bigevent_percentage = settings.rrg["bigevent_percentage"]; #portion of cell population to replace
    bigevent_percentage_stdv = settings.rrg["bigevent_percentage_stdv"]; #standard deviation (to randomize the portion of cell population to replace)
    
    if deterministic_bigevent:
        bigevent_times = settings.rrg["bigevent_times"];
        bigevent_settings = (deterministic_bigevent,bigevent_with_driver,bigevent_parents,bigevent_times , bigevent_percentage, bigevent_percentage_stdv);
    else:
        bigevent_chance = settings.rrg["bigevent_chance"]; #per cell per time (only one cell can have big event)
        bigevent_settings = (deterministic_bigevent,bigevent_with_driver,bigevent_parents,bigevent_chance, bigevent_percentage, bigevent_percentage_stdv);
    
    timepointN =settings.rrg["timepointN"];
    timepoints = settings.rrg["timepoints"];
    timepointN = min(timepointN,5); #max regularly spaced samples
    if timepointN != 0:
        timepoints = [i for i in range(timeT//(timepointN+1),timeT,timeT//(timepointN))][:timepointN];
        measurementID = 0;
    else:
        if len(timepoints) != 0:
            measurementID = 0;
        else:
            measurementID = -1;
    
    # =============================================================================
    # Set initial reads and allocate memory
    # =============================================================================
    
    #make, tag and allocate memory for all reads
    def InitializeReads(timeT,cellN,nuclN,init_reads = None,initial_mutation_fraction = .1):
        
        #retag initial reads or make new initial reads and mutate them TODO: no retagging when new time event
        if init_reads != None:
            for readID in range(len(init_reads)):
                init_reads[readID] = [readID] + init_reads[readID][1:];
        else:
            init_reads = [[0] + [0 for n in range(nuclN)] for i in range(cellN)];
            
            #introduce random SNPs
            init_mut,mut_mask = MutBool(np.ones(nuclN)*initial_mutation_fraction);
            #mutate initial reads, all of these mutations will be the VAF peaks of 1 in the final clustering
            for read in init_reads:
                read[1:] = list(mut_mask | np.array(read[1:], dtype=bool));
                
        Dbg("Initial reads:");
        PrintReads(init_reads,range(cellN));
        Dbg("");
        
        #allocate memory for cellN_high_bound
        reads = [[np.zeros(nuclN) for cell in range(cellN)] for time in range(timeT)];
        reads[0] = init_reads;
        
        #lists to track the phylogeny and mutations
        cell_muts = [[None for cell in range(cellN)] for time in range(timeT)];
        cell_parents = [[None for cell in range(cellN)] for time in range(timeT)];
        drivers = [None]*driverMutN; #will record the initial times for driver mutation
        
        return reads,cell_muts,cell_parents,drivers;
        
    
    # =============================================================================
    # Go through time loop, divide and mutate reads
    # =============================================================================
    
    t = 0; #time within one measurement
    
    bigeventID = 0; #-1 = no big events yet, 0:end is the index of the current bigevent
    bigevent_cellIDs = []; #the cell ids of the cells that got a bigevent in chronological order
    bigevent_cell_popparent = []; #the population (read tag) of the cells with the big event in chronological order
    bigexcludes = []; #the occured bigevent mutatations, which are excluded in later bigevents to conform with ISA
    all_prevalences = []; #the cellular prevalences of each population/subclone based on nucleotide tag
    
    #TODO: merge with loop
    #if multiple measurements then start partial log and set intermediate endtime
    if measurementID == 0:
        Dbg("Measurement 0");
    
    #make reads and other vars for: t = 0;
    reads,cell_muts,cell_parents,drivers = InitializeReads(timeT+1,cellN,nuclN,None,settings.rrg["initial_mutation_fraction"]);
    
    #generate mutation-time-matrix (all mutations for all times for all cells for all loci)
    if nuclN < 1000:
      initial_mut_mat,initial_mut_mask_mat = MutBool(mutRate, time_number = timeT, cell_number = cellN);
    else:
      initial_mut_mat,initial_mut_mask_mat = [[None]*timeT,[None]*timeT];
    
    #make reads and other vars for: t = 1 up to and including timeT-1
    while(t<timeT):
        if LOADBAR:
            Loadbar(t, timeT);
            ContinueLog(LOGNAME);
        
        #duplicate, select and possibly mutate cells
        #keep record of cell mutations (cell_muts) and cell parents (cell_parents)
        cell_muts[t+1], cell_parents[t+1], reads[t+1], drivers, bigevent, bigeventID, bigexcludes = DuplicateHaploid(
            reads[t],
            t,
            cellN,
            cell_muts[t],
            cell_parents[t],
            mutRate,
            initial_mut_mat[t],
            initial_mut_mask_mat[t],
            drivers,
            driverMuts,
            bigeventID,
            bigevent_settings,
            bigexcludes,
            selectionMask,
            seed_gen,
        )
        
        #find cellular prevalence (subclonal percentage)
        bigeventN = len(bigevent_times);
        prevalences = GetPrevalence(reads[t+1],bigeventN);
        all_prevalences.append(prevalences);
        
        #if big event just happened then record the cell and its parent
        if bigevent != -1:
            bigevent_cellIDs.append(bigevent);
            bigevent_parent = cell_parents[t][bigevent];
            
            if bigevent_parent != None:
                print("PopParent of bigevent: ",reads[t-1][bigevent_parent][0])
                bigevent_cell_popparent.append(reads[t-1][bigevent_parent][0]);
            else:
                print("PopParent of bigevent: ",0)
                bigevent_cell_popparent.append(0);
        
        PrintTimeBlock(reads[t+1],t+1);
        
        #at requested timepoints "do measurement" and export data
        if (t+1 in timepoints):
            Dbg("Log measurement");
            
            sys.stdout = open(PATH+"reads_data_" + str(measurementID) + ".txt", "w");
            ExportReads(reads[t+1]);
            ContinueLog(LOGNAME);
            
            measurementID += 1;
            
        t += 1;
    
    if LOADBAR:
        Loadbar(t, timeT);
        ContinueLog(LOGNAME);
    
    EndLog(reads,timeT,cellN,cell_muts,cell_parents,driverMuts);
    
    Dbg("____________________________________\nReads at final time " + str(t));
    
    end = time.time();
    
    print("\tdone");
    print("\tgeneration time: " + str(end - start));
    
    # =============================================================================
    # Data is generated, shuffle and do random read amplification, then output endtime-reads to txt file
    # =============================================================================
        
    all_last_reads = [];
    
    for cell in reads[-1]:
        all_last_reads.append(list(cell));
    
    #shuffle last reads for good measure (make positional info unavailable) if no spatial sampling 
    if settings.rrg["spatialN"] == 0:
      rnd.shuffle(all_last_reads);
        
    #check the tags of the last reads
    tag_set = set();
    for read in all_last_reads:
        tag_set.add(read[0]);
    Dbg("Set of all tags in last reads: " + str(tag_set));
    Dbg("");
    
    if FISH:
        sys.stdout = open(PATH+"prevalences.txt", "w");
        #print the measurement times (for vertical fishplot lines)
        if len(timepoints) != 0:
            for timepoint in timepoints[:-1]:
                print(timepoint, end=",");
            print(timepoints[-1]);
        else:
            print(timeT);
        
        #print the population (parent) of the big event cells (for phylogenetic reconstruction)
        for pop_parent in bigevent_cell_popparent[:-1]:
            print(pop_parent, end=",");
        print(bigevent_cell_popparent[-1]);
        
        #print the prevalence of the populations (for phylogenetic reconstruction)
        for prevalences in all_prevalences:
            for prevalence in prevalences[:-1]:
                print(prevalence, end=",");
            print(prevalences[-1]);
        sys.stdout = sys.__stdout__;
    
    #final reads output (either pipe or to file)
    if PIPE:
        sys.stdout = sys.__stdout__;
    elif settings.rrg["spatialN"] == 0:
        sys.stdout = open(PATH+"reads_data.txt", "w"); #if not in output folder add "[:-(len("output")+1)]" behind "PATH"
    
    #check if spatial samples need to be taken and then write out (to file/pipe)
    if settings.rrg["spatialN"] == 0:
      ExportReads(all_last_reads);
    else:
      ExportReads(all_last_reads,settings.rrg["spatialN"],settings.rrg["spatial_fraction"]);
    
    if seedfile != None:
      seedfile.close();
        
if __name__ == '__main__':
    from settings import settings;
    
    process_id = None;
    if len(sys.argv) == 2:
        process_id = int(sys.argv[1]);
        
    main(settings,process_id);
    
