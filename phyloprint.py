# -*- coding: utf-8 -*-
"""
Created on Tue Feb  4 20:23:22 2020

@author: niels
"""
from anytree import Node, RenderTree
from settings import general;
from numpy.random import ranf;
import sys;

DEBUG = general["DEBUG"];
DEBUG_READS = general["DEBUG_READS"];
DEBUG_PHYLO = general["DEBUG_PHYLO"];
PATH = general["PATH"];
LOGNAME = general["LOGNAME"];
LOG = general["LOG"];

def Dbg(s):
    if DEBUG:
        print(s);

#start a partial log
def StartLog(name):
    if LOG:
        sys.stdout = open(PATH+name+".txt", "w");

def ContinueLog(name):
    if LOG:
        sys.stdout = open(PATH+name+".txt", "a");

#end a partial log and append it to the main log
def EndLog(reads,timeT,cellN,cell_muts,cell_parents,driverMuts):
    if LOG:
        Dbg("Final time: "+str(timeT));
        PrintReads(reads[timeT],range(cellN));
            
        Dbg("\nDriver mutations again:")
        Dbg(driverMuts);
        
        Dbg("\nCell muts: ")
        for cell_mutID in range(len(cell_muts)):
            Dbg(str(cell_mutID) + str(cell_muts[cell_mutID]));
        Dbg("\nParent cells: ");
        for cell_parentID in range(len(cell_parents)):
            Dbg(str(cell_parentID) + str(cell_parents[cell_parentID]));
            
            
        #print out driver mutation cells
        Dbg("\nDriver mutations aquired after cells:")
        for t in range(len(cell_muts)):
            for cell in range(len(cell_muts[t])):
                if cell_muts[t][cell] != None:
                    for mut in cell_muts[t][cell]:
                         if mut in driverMuts:
                            Dbg((t-1,cell));
                            
        #transform mutation record to a tree using anytree
        Dbg("");
        
        PhyloPrint(cell_parents);
        
def AppendPartialLog(name): 
    log = open(PATH+LOGNAME+".txt", "a");
    partial_log = open(PATH+name+".txt","r");
    log.write("".join(partial_log.readlines()));
    log.close();
    partial_log.close();

def PhyloPrint(cell_parents):
    if DEBUG_PHYLO:
        phylo_nodes = {}; #dictionairy of all the nodes in the tree
        ancestor_nodes = [];
        
        #initial parent search
        for t in range(len(cell_parents)):
            for cell in range(len(cell_parents[t])):
                #if had any parent with a mutation register in tree
                if cell_parents[t][cell] == None:
                    #if parent has no parents, this parent is MRCA, add this parent without having a parent of itself
                    #MRCA is added to list
                    ancestor = MakeNode((t,cell));
                    ancestor_nodes.append(ancestor);
                    phylo_nodes[(t,cell)] = ancestor;
                else:
                    par_t = t-1;
                    par = cell_parents[t][cell]; #time that parent had mutation and its cellID
                    
                    #make new node, add pareqnt (previous_nodes[cellID of previous node that was inherited from])
                    new_node = MakeNode((t,cell));
                    new_node.parent = phylo_nodes[(par_t,par)];
                    phylo_nodes[(t,cell)] = new_node;
        
        for ancestor_node in ancestor_nodes:
            print("Phylogeny for initial cell " + ancestor_node.name + ": ");
            for pre, fill, node in RenderTree(ancestor_node):
                print("%s%s" % (pre, node.name));

def MakeNode(time_cell):
    t = time_cell[0];
    cell = time_cell[1];
    
    node_name = str(t) + "-" + str(cell);
    
    return Node(node_name);

def Binary(a):
    return list(map(int,a));

def BinaryVisual(a):
    binstr = str(a[0]);
    for e in a[1:]:
        if e == True:
            binstr += '█';
        elif e == False:
            binstr += '─';
    return binstr;

def dim(a):
    if not type(a) == list:
        return []
    return [len(a)] + dim(a[0]);

def PrintReads(reads,cells):
    if DEBUG_READS:
        print("        ", end="");
        for i in range(len(reads[0])-1):
            print(i%10, end="");
        print("");
        #for every cell i
        for i in cells:
            print("Cell " + str(i), end = ":");
            
            #for every chromosome j
            print(BinaryVisual(reads[i]));

def ExportReads(reads,spatialN = None, spatial_fraction = None):
    
    if spatialN == None or spatialN == 0:
        #column for R colnames
        for i in range(1,len(reads[0])):
            print(str(i),end=",");
        print(len(reads[0]));
        
        #print all the reads (without tag) seperated by comma
        for readID in range(len(reads)):
            read = reads[readID];
            for pos in read[1:-1]:
                print(str(int(pos)),end=",");
            print(str(int(read[-1])));
            
    elif spatialN > 0 and spatial_fraction > 0:
        for i in range(spatialN):
            sys.stdout = open(PATH+"reads_data_" + str(i) + ".txt", "w");
            
            #column for R colnames
            for i in range(1,len(reads[0])):
                print(str(i),end=",");
            print(len(reads[0]));
            
            sample_fraction_start = ranf()*(1-(spatial_fraction));
            sample_start = round(sample_fraction_start*len(reads));
            sample_end = round((sample_fraction_start+spatial_fraction)*len(reads));
            
            for readID in range(sample_start,sample_end):
                read = reads[readID];
                for pos in read[1:-1]:
                    print(str(int(pos)),end=",");
                print(str(int(read[-1])));
            
    else:
        print("Spatial sampling parameters have not been well defined by user");

def PrintTimeBlock(reads,t):
    Dbg("\t\t\t\t\t\t_______");
    Dbg("\t\t\t\t\t\tTime: " + str(t) + " (over all measurements)");
    Dbg("\t\t\t\t\t\t_______");
    
    Dbg("____________________________________\nReads at time (in this measurement) " + str(t));
    if DEBUG:
        PrintReads(reads,range(len(reads)));
        pass;
    
    Dbg("____________________________________\n");

